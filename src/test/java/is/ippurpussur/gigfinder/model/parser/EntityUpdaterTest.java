package is.ippurpussur.gigfinder.model.parser;

import is.ippurpussur.gigfinder.model.neo4j.*;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public final class EntityUpdaterTest {

    private EntityUpdater entityUpdater;

    @Before
    public void createObjects() {
        entityUpdater = new EntityUpdater();
    }

    @Test
    public void shouldFindAreaToUpdate() throws Exception {
        final Area areaWithWrongCoordinates = new Area(COORDINATES_2, STREET, ZIP, CITY, null, COUNTRY);
        final Area areaWithoutCoordinates = new Area(null, STREET, ZIP, CITY, null, COUNTRY);
        final Area areaWithoutCoordinatesAndZip = new Area(null, STREET, null, CITY, null, COUNTRY);

        final List<Area> areas = Collections.singletonList(AREA);
        final List<Area> areas2 = Collections.singletonList(areaWithWrongCoordinates);
        final List<Area> areas3 = Collections.singletonList(areaWithoutCoordinates);
        final List<Area> areas4 = Collections.singletonList(areaWithoutCoordinatesAndZip);
        final List<Area> areas5 = Collections.singletonList(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP);

        final Area areaToUpdate = entityUpdater.findAreaToUpdate(areas, AREA);
        final Area areaToUpdate2 = entityUpdater.findAreaToUpdate(areas2, AREA);
        final Area areaToUpdate3 = entityUpdater.findAreaToUpdate(areas3, AREA);
        final Area areaToUpdate4 = entityUpdater.findAreaToUpdate(areas4, AREA);
        final Area areaToUpdate5 = entityUpdater.findAreaToUpdate(areas5, AREA);

        assertThat(areaToUpdate, equalTo(AREA));
        assertThat(areaToUpdate2, is(nullValue()));
        assertThat(areaToUpdate3, equalTo(areaWithoutCoordinates));
        assertThat(areaToUpdate4, equalTo(areaWithoutCoordinatesAndZip));
        assertThat(areaToUpdate5, is(nullValue()));
    }

    @Test
    public void shouldFindEventToUpdate() throws Exception {
        final List<Artist> wrongArtists = Collections.singletonList(ARTIST_MUM);

        final Venue wrongVenue = new Venue(VENUE);
        wrongVenue.setArea(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP);

        final Event eventWithWrongArtist = new Event(EVENT);
        eventWithWrongArtist.setArtists(wrongArtists);

        final Event eventWithWrongVenue = new Event(EVENT);
        eventWithWrongVenue.setVenue(wrongVenue);

        final Event eventWithOtherStartTime = new Event(EVENT);
        eventWithOtherStartTime.setStartTime(ANOTHER_START_TIME);

        final List<Event> events = Collections.singletonList(EVENT);
        final List<Event> events2 = Collections.singletonList(eventWithWrongArtist);
        final List<Event> events3 = Collections.singletonList(eventWithWrongVenue);
        final List<Event> events4 = Collections.singletonList(eventWithOtherStartTime);
        final List<Event> events5 = Arrays.asList(EVENT, eventWithOtherStartTime);

        final Event eventToUpdate = entityUpdater.findEventToUpdate(events, EVENT);
        final Event eventToUpdate2 = entityUpdater.findEventToUpdate(events2, EVENT);
        final Event eventToUpdate3 = entityUpdater.findEventToUpdate(events3, EVENT);
        final Event eventToUpdate4 = entityUpdater.findEventToUpdate(events4, EVENT);
        final Event eventToUpdate5 = entityUpdater.findEventToUpdate(events5, EVENT);

        assertThat(eventToUpdate, equalTo(EVENT));
        assertThat(eventToUpdate2, is(nullValue()));
        assertThat(eventToUpdate3, is(eventToUpdate3));
        assertThat(eventToUpdate4, is(nullValue()));
        assertThat(eventToUpdate5, equalTo(EVENT));
    }

    @Test
    public void shouldFindStartTimeToUpdate() throws Exception {
        final List<StartTime> startTimes = Collections.singletonList(START_TIME);
        final List<StartTime> startTimes2 = Collections.singletonList(ANOTHER_START_TIME);

        final StartTime startTimeToUpdate = entityUpdater.findStartTimeToUpdate(startTimes, START_TIME);
        final StartTime startTimeToUpdate2 = entityUpdater.findStartTimeToUpdate(startTimes2, START_TIME);

        assertThat(startTimeToUpdate, equalTo(START_TIME));
        assertThat(startTimeToUpdate2, is(nullValue()));
    }

    @Test
    public void findArtistToUpdate() throws Exception {
        final Artist artistWithOtherWebsite = new Artist(ARTIST_PETER_BRODERICK.getName(), ARTIST_PETER_BRODERICK.getMbid());
        artistWithOtherWebsite.setWebsite("peter.com");

        final List<Artist> artists2 = Collections.singletonList(ARTIST_MUM);
        final List<Artist> artists3 = Collections.singletonList(artistWithOtherWebsite);

        final Map<Artist, Integer> numberOfEvents = Collections.singletonMap(ARTIST_PETER_BRODERICK, 1);

        final Map<Artist, Integer> numberOfEventsWithValue4 = Collections.singletonMap(ARTIST_PETER_BRODERICK, 4);

        final Artist artistToUpdate = entityUpdater.findArtistToUpdate(ARTISTS, ARTIST_PETER_BRODERICK, numberOfEvents);
        final Artist artistToUpdate2 = entityUpdater.findArtistToUpdate(artists2, ARTIST_PETER_BRODERICK, numberOfEvents);
        final Artist artistToUpdateWithManyEevnts =
                entityUpdater.findArtistToUpdate(ARTISTS, ARTIST_PETER_BRODERICK, numberOfEventsWithValue4);
        final Artist artistToUpdateWithOtherWebsiteAndManyEvents =
                entityUpdater.findArtistToUpdate(artists3, ARTIST_PETER_BRODERICK, numberOfEventsWithValue4);

        assertThat(artistToUpdate, equalTo(ARTIST_PETER_BRODERICK));
        assertThat(artistToUpdate2, is(nullValue()));
        assertThat(artistToUpdateWithManyEevnts, equalTo(ARTIST_PETER_BRODERICK));
        assertThat(artistToUpdateWithOtherWebsiteAndManyEvents, is(nullValue()));
    }

    @Test
    public void shouldUpdateArea() throws Exception {
        final Area updatedArea = entityUpdater.updateArea(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP, AREA, COORDINATES);

        assertThat(updatedArea, equalTo(AREA));
    }

    @Test
    public void shouldUpdateStartTime() throws Exception {
        final StartTime updatedStartTime = entityUpdater.updateStartTime(ANOTHER_START_TIME, START_TIME);

        assertThat(updatedStartTime, equalTo(START_TIME));
    }

    @Test
    public void shouldUpdateCoordinates() throws Exception {
        final Coordinates updatedCoordinates = entityUpdater.updateCoordinates(COORDINATES_2, COORDINATES);

        assertThat(updatedCoordinates, equalTo(COORDINATES));
    }

    @Test
    public void shouldUpdateVenue() throws Exception {
        final Venue venueToUpdate = new Venue(
                "Handelsbeurs",
                AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP,
                null,
                null,
                null,
                false
        );

        final Venue venueToUpdateCopy = new Venue(venueToUpdate);

        final Area otherArea = new Area(
                new Coordinates(51.0498458, 3.7225496, true),
                "Kouter 29",
                "9000",
                "Ghent",
                null,
                "Belgium"
        );

        final Venue updatedVenue = entityUpdater.updateVenue(venueToUpdate, VENUE, AREA);
        final Venue updatedVenueWithOtherArea = entityUpdater.updateVenue(venueToUpdateCopy, VENUE, otherArea);

        assertThat(updatedVenue, equalTo(VENUE));
        assertThat(updatedVenueWithOtherArea.getArea(), equalTo(otherArea));
        assertThat(updatedVenueWithOtherArea.getWebsite(), equalTo(VENUE.getWebsite()));
        assertThat(updatedVenueWithOtherArea.getPhone(), equalTo(VENUE.getPhone()));
        assertThat(updatedVenueWithOtherArea.getDescription(), equalTo(VENUE.getDescription()));
        assertThat(updatedVenueWithOtherArea.getName(), equalTo(VENUE.getName()));
    }

    @Test
    public void shouldUpdateEvent() throws Exception {
        final Event eventCopy = new Event(EVENT);

        final Event eventCreatedFromBandsintown = new Event(
                null,
                13702385L,
                ANOTHER_EVENT_NAME,
                ARTISTS,
                VENUE,
                ANOTHER_START_TIME
        );

        final Event eventCreatedFromSongkick = new Event(
                SONGKICK_EVENT_ID,
                null,
                ANOTHER_EVENT_NAME,
                ARTISTS,
                VENUE,
                ANOTHER_START_TIME
        );

        final Event updatedEvent = entityUpdater.updateEvent(EVENT, eventCreatedFromBandsintown, ARTISTS, VENUE, ANOTHER_START_TIME);
        final Event updatedEventCreatedFromSongkickData =
                entityUpdater.updateEvent(eventCopy, eventCreatedFromSongkick, ARTISTS, VENUE, ANOTHER_START_TIME);
        final Event updatedEventCreatedFromBandsintown = entityUpdater.updateEvent(
                eventCreatedFromBandsintown,
                eventCreatedFromSongkick,
                Collections.singletonList(ARTIST_MUM),
                VENUE,
                START_TIME
        );

        assertThat(updatedEvent.getArtists(), equalTo(ARTISTS));
        assertThat(updatedEvent.getName(), equalTo(EVENT_NAME));
        assertThat(updatedEvent.getVenue(), equalTo(VENUE));
        assertThat(updatedEvent.getBandsintownId(), is(eventCreatedFromBandsintown.getBandsintownId()));
        assertThat(updatedEvent.getSongkickId(), is(SONGKICK_EVENT_ID));
        assertThat(updatedEvent.getStartTime(), equalTo(START_TIME));

        assertThat(updatedEventCreatedFromSongkickData.getName(), equalTo(EVENT_NAME));
        assertThat(updatedEventCreatedFromSongkickData.getSongkickId(), equalTo(eventCopy.getSongkickId()));
        assertThat(updatedEventCreatedFromSongkickData.getBandsintownId(), equalTo(eventCopy.getBandsintownId()));

        assertThat(updatedEventCreatedFromBandsintown.getStartTime(), equalTo(START_TIME));
        assertThat(updatedEventCreatedFromBandsintown.getArtists(), hasSize(1));
        assertThat(updatedEventCreatedFromBandsintown.getArtists(), hasItem(ARTIST_MUM));
    }
}