package is.ippurpussur.gigfinder.model.parser;

import is.ippurpussur.gigfinder.model.neo4j.Event;
import is.ippurpussur.gigfinder.model.songkick.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public final class SongkickParserTest {

    @Test
    public void shouldConvertSongkickEventsToNodeEntities() throws Exception {
        final Identifier peterIdentifier = new Identifier(PETER_BRODERICK_MBID);
        final List<Identifier> peterIdentifiers = Collections.singletonList(peterIdentifier);

        final Identifier mumIdentifier = new Identifier(MUM_MBID);
        final List<Identifier> mumIdentifiers = Collections.singletonList(mumIdentifier);

        final Performance peterPerformance = new Performance(new SongkickArtist(PETER_BRODERICK_NAME, peterIdentifiers));
        final Performance mumPerformance = new Performance(new SongkickArtist(MUM_NAME, mumIdentifiers));
        final List<Performance> performanceList = Arrays.asList(peterPerformance, mumPerformance);

        final SongkickVenue venue = new SongkickVenue(
                111111L,
                VENUE_NAME,
                LATITUDE,
                LONGITUDE,
                STREET,
                ZIP,
                new SongkickCity(CITY, new Country(COUNTRY)),
                null,
                null,
                null
        );

        final SongkickEvent songkickEvent = new SongkickEvent(SONGKICK_EVENT_ID, EVENT_NAME, performanceList, venue, START_TIME);
        final SongkickEvent songkickEvent2 =
                new SongkickEvent(29023889L, ANOTHER_EVENT_NAME, performanceList, venue, START_TIME);
        final List<SongkickEvent> songkickEvents = Collections.singletonList(songkickEvent);
        final List<SongkickEvent> songkickEvents2 = Arrays.asList(songkickEvent, songkickEvent2);

        final String languageCode = "en";

        SongkickParser parser = new SongkickParser();
        final List<Event> events = parser.convertSongkickEventsToNodeEntities(songkickEvents, languageCode);
        final List<Event> events2 = parser.convertSongkickEventsToNodeEntities(songkickEvents2, languageCode);

        assertThat(events, hasSize(1));
        assertThat(events.get(0).getBandsintownId(), is(nullValue()));
        assertThat(events.get(0).getVenue().getName(), equalTo(VENUE_NAME));
        assertThat(events.get(0).getVenue().getDescription(), is(nullValue()));
        assertThat(events.get(0).getVenue().getWebsite(), is(nullValue()));
        assertThat(events.get(0).getVenue().getPhone(), is(nullValue()));
        assertThat(events.get(0).getVenue().getArea().getCountry(), equalTo(COUNTRY));
        assertThat(events.get(0).getVenue().getArea().getStreet(), equalTo(STREET));
        assertThat(events.get(0).getVenue().getArea().getZip(), equalTo(ZIP));
        assertThat(events.get(0).getVenue().getArea().getCoordinates().getLatitude(), equalTo(LATITUDE));
        assertThat(events.get(0).getVenue().getArea().getCoordinates().getLongitude(), equalTo(LONGITUDE));
        assertThat(events.get(0).getVenue().getArea().getCity(), equalTo(CITY));
        assertThat(events.get(0).getVenue().getDescription(), is(nullValue()));

        assertThat(events2, hasSize(2));
        assertThat(events2.get(0), not(equalTo(events2.get(1))));
    }
}