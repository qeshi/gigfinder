package is.ippurpussur.gigfinder.model.parser;

import is.ippurpussur.gigfinder.model.lastfm.SimilarArtist;
import is.ippurpussur.gigfinder.model.neo4j.Area;
import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.Venue;
import org.junit.Test;

import java.util.Map;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public final class DomainParserTest {

    private final DomainParser domainParser = new DomainParser();

    @Test
    public void shouldConvertSimilarArtistToArtist() throws Exception {
        final String name = "Last Days";
        final String mbid = "c356cf11-d7c9-46bf-974a-d34da4273127";
        final Double matchLevel = 0.527903;
        final SimilarArtist similarArtist = new SimilarArtist(name, mbid, matchLevel);

        final Artist artist = domainParser.convertSimilarArtistToArtist(similarArtist);

        assertThat(artist.getWebsite(), is(nullValue()));
        assertThat(artist.getMbid(), equalTo(mbid));
        assertThat(artist.getName(), equalTo(name));
    }

    @Test
    public void shouldFormatCoordinate() throws Exception {
        final Double formattedCoordinate = domainParser.formatCoordinate(LATITUDE);
        final Double formatteaCoordinate2 = domainParser.formatCoordinate(LONGITUDE);

        assertThat(formattedCoordinate, is(FORMATTED_LATITUDE));
        assertThat(formatteaCoordinate2, is(FORMATTED_LONGITUDE));
    }

    @Test
    public void shuoldFormatCoordinates() throws Exception {
        final Map<String, Double> formattedCoordinates = domainParser.formatCoordinates(LATITUDE, LONGITUDE);

        assertThat(formattedCoordinates.get(Key.LAT), is(FORMATTED_LATITUDE));
        assertThat(formattedCoordinates.get(Key.LNG), is(FORMATTED_LONGITUDE));
    }

    @Test
    public void shouldSetAllowableRangeForCoordinates() throws Exception {
        final Double errorRange = 0.1;
        final Map<String, Double> coordinatesWithinAllowableRange =
                domainParser.setAllowableRangeForCoordinates(LATITUDE, LONGITUDE, errorRange);
        final Double minLatitude = coordinatesWithinAllowableRange.get(Key.MIN_LAT);
        final Double maxLatitude = coordinatesWithinAllowableRange.get(Key.MAX_LAT);
        final Double minLongitude = coordinatesWithinAllowableRange.get(Key.MIN_LNG);
        final Double maxLongitude = coordinatesWithinAllowableRange.get(Key.MAX_LNG);

        final Double formattedMinLat = domainParser.formatCoordinate(LATITUDE - errorRange);
        final Double formattedMaxLat = domainParser.formatCoordinate(LATITUDE + errorRange);
        final Double formattedMinLng = domainParser.formatCoordinate(LONGITUDE - errorRange);
        final Double formattedMaxLng = domainParser.formatCoordinate(LONGITUDE + errorRange);

        assertThat(minLatitude, is(formattedMinLat));
        assertThat(maxLatitude, is(formattedMaxLat));
        assertThat(minLongitude, is(formattedMinLng));
        assertThat(maxLongitude, is(formattedMaxLng));
    }

    @Test
    public void stringShouldHaveValue() throws Exception {
        final String hello = "Hello";
        final String empty = "     ";
        final String stringWithSpace = "    SPACE  ";

        final boolean helloHasValue = domainParser.stringHasValue(hello);
        final boolean emptyHasValue = domainParser.stringHasValue(empty);
        final boolean stringWithSpaceHasValue = domainParser.stringHasValue(stringWithSpace);

        assertThat(helloHasValue, is(true));
        assertThat(emptyHasValue, is(false));
        assertThat(stringWithSpaceHasValue, is(true));
    }

    @Test
    public void venuesShouldBeSame() throws Exception {
        final String anotherName = "L'Ubu";
        final String differentName = "Uno";
        final Area areaWithNullCoordinatesAndNullZip = new Area(null, STREET, null, CITY, null, COUNTRY);
        final Area areaWithZip = new Area(null, STREET, ZIP, CITY, null, COUNTRY);
        final Venue venueWithAnotherName = new Venue(VENUE);
        venueWithAnotherName.setName(anotherName);

        final Venue venueWithOtherCoordinatesAndNullZip = new Venue(VENUE);
        venueWithOtherCoordinatesAndNullZip.setArea(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP);
        venueWithOtherCoordinatesAndNullZip.setLatLng(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP.getLatLng());

        final Venue venueWithNullCoordinatesAndNullZip = new Venue(VENUE);
        venueWithNullCoordinatesAndNullZip.setArea(areaWithNullCoordinatesAndNullZip);
        venueWithNullCoordinatesAndNullZip.setLatLng(areaWithNullCoordinatesAndNullZip.getLatLng());

        final Venue venueWithDifferentNameAndNullCoordinates = new Venue(VENUE);
        venueWithDifferentNameAndNullCoordinates.setName(differentName);
        venueWithDifferentNameAndNullCoordinates.setArea(areaWithNullCoordinatesAndNullZip);
        venueWithDifferentNameAndNullCoordinates.setLatLng(areaWithNullCoordinatesAndNullZip.getLatLng());

        final Venue venueWithZip = new Venue(VENUE);
        venueWithZip.setArea(areaWithZip);

        final boolean venuesAreSame = domainParser.venuesAreSame(VENUE, venueWithAnotherName);
        final boolean venuesAreSame2 = domainParser.venuesAreSame(VENUE, venueWithOtherCoordinatesAndNullZip);
        final boolean venuesAreSame3 = domainParser.venuesAreSame(VENUE, venueWithNullCoordinatesAndNullZip);
        final boolean venuesAreSame4 = domainParser.venuesAreSame(VENUE, venueWithDifferentNameAndNullCoordinates);
        final boolean venuesAreSame5 = domainParser.venuesAreSame(VENUE, venueWithZip);

        assertThat(venuesAreSame, is(true));
        assertThat(venuesAreSame2, is(false));
        assertThat(venuesAreSame3, is(true));
        assertThat(venuesAreSame4, is(false));
        assertThat(venuesAreSame5, is(true));
    }

    @Test
    public void shouldSimplifyEventName() throws Exception {
        final String festivalName = "Fuji Rock Festival at Mt.Fuji (August 14, 2017)";

        final String simplifiedEventName = domainParser.simplifyEventName(EVENT_NAME);
        final String simplifiedFestivalName = domainParser.simplifyEventName(festivalName);

        assertThat(simplifiedEventName, equalTo("Peter Broderick"));
        assertThat(simplifiedFestivalName, equalTo("Fuji Rock Festival"));
    }
}
