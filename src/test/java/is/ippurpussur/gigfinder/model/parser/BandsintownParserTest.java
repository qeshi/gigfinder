package is.ippurpussur.gigfinder.model.parser;

import is.ippurpussur.gigfinder.model.bandsintown.BandsintownEvent;
import is.ippurpussur.gigfinder.model.bandsintown.BandsintownVenue;
import is.ippurpussur.gigfinder.model.neo4j.Area;
import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.Event;
import is.ippurpussur.gigfinder.neo4j.service.AreaService;
import is.ippurpussur.gigfinder.restclient.googlegeocoder.GeocoderAPIImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class BandsintownParserTest {

    private BandsintownParser bandsintownParser;

    @Mock
    private AreaService areaServiceMock;

    @Before
    public void setUp() throws Exception {
        bandsintownParser = new BandsintownParser();
        MockitoAnnotations.initMocks(AreaService.class);
    }

    @Test
    public void shouldConvertBandsintownEventToNodeEntity() throws Exception {
        final List<Artist> artists = Arrays.asList(ARTIST_MUM, ARTIST_PETER_BRODERICK);
        final Long bandsintownId = 1L;

        final BandsintownEvent bandsintownEvent = new BandsintownEvent(
                bandsintownId,
                EVENT_NAME,
                DATE_TIME,
                artists,
                new BandsintownVenue(VENUE_NAME, CITY, COUNTRY, LATITUDE, LONGITUDE)
        );

        final List<Area> areas = Collections.singletonList(AREA);
        when(areaServiceMock.findByFormattedCoordinates(FORMATTED_LATITUDE, FORMATTED_LONGITUDE)).thenReturn(areas);

        final Event event =
                bandsintownParser.convertBandsintownEventToNodeEntity(bandsintownEvent, areaServiceMock, "en");

        assertThat(event.getBandsintownId(), equalTo(bandsintownId));
        assertThat(event.getName(), equalTo(EVENT_NAME));
        assertThat(event.getArtists().size(), is(artists.size()));
        assertThat(event.getArtists(), hasItems(ARTIST_MUM, ARTIST_PETER_BRODERICK));
        assertThat(event.getSongkickId(), is(nullValue()));
        assertThat(event.getStartTime().getDate(), equalTo(DATE));
        assertThat(event.getStartTime().getTime(), equalTo(TIME));
        assertThat(event.getStartTime().getDateTime(), equalTo(DATE_TIME));
        assertThat(event.getVenue().getName(), equalTo(VENUE_NAME));
        assertThat(event.getVenue().getArea().getCity(), equalTo(CITY));
        assertThat(event.getVenue().getArea().getCoordinates().getLatitude(), equalTo(LATITUDE));
        assertThat(event.getVenue().getArea().getCoordinates().getLongitude(), equalTo(LONGITUDE));
        assertThat(event.getVenue().getArea().getCoordinates().getFormattedLatitude(), equalTo(FORMATTED_LATITUDE));
        assertThat(event.getVenue().getArea().getCoordinates().getFormattedLongitude(), equalTo(FORMATTED_LONGITUDE));
        assertThat(event.getVenue().getArea().getCountry(), equalTo(COUNTRY));
        assertThat(event.getVenue().getArea().getStreet(), is(nullValue()));
        assertThat(event.getVenue().getArea().getZip(), is(nullValue()));
        assertThat(event.getVenue().getDescription(), is(nullValue()));
        assertThat(event.getVenue().getPhone(), is(nullValue()));
        assertThat(event.getVenue().getWebsite(), is(nullValue()));
    }
}
