package is.ippurpussur.gigfinder;

import is.ippurpussur.gigfinder.model.neo4j.*;

import java.util.Collections;
import java.util.List;

public final class TestData {

    public static final String MUM_NAME = "múm";
    public static final String MUM_MBID = "2b64e811-cbcd-45f7-b29d-296444cad4f1";
    public static final Artist ARTIST_MUM = new Artist(MUM_NAME, MUM_MBID);

    public static final String PETER_BRODERICK_NAME = "Peter Broderick";
    public static final String PETER_BRODERICK_MBID = "e60a4481-472a-42cf-a84d-6a9419e4e5e3";
    public static final Artist ARTIST_PETER_BRODERICK = new Artist(PETER_BRODERICK_NAME, PETER_BRODERICK_MBID);

    public static final Artist ARTIST_ALBUM_LEAF = new Artist("The Album Leaf", "ffb18e19-64a4-4a65-b4ce-979e00c3c69d");
    public static final Artist ARTIST_JOHAN_JOHANSSON =
            new Artist("Jóhan Jóhansson", "578c1e77-4756-44dd-86a9-b1e71a5b635e");


    public static final Double LATITUDE = 48.1075006;
    public static final Double LONGITUDE = -1.6722639;
    private static final Double LATITUDE_2 = 46.1075006;

    public static final Double FORMATTED_LATITUDE = 48.1;
    public static final Double FORMATTED_LONGITUDE = -1.6;

    public static final Coordinates COORDINATES = new Coordinates(LATITUDE, LONGITUDE, true);
    public static final Coordinates COORDINATES_2 = new Coordinates(LATITUDE_2, LONGITUDE, true);

    public static final String STREET = "1 Rue Saint-Helier";
    public static final String ZIP = "35000";
    public static final String CITY = "Rennes";
    public static final String CITY_2 = "Paris";
    public static final String COUNTRY = "France";
    public static final Area AREA = new Area(COORDINATES, STREET, ZIP, CITY, null, COUNTRY);
    public static final Area AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP =
            new Area(COORDINATES_2, STREET, null, CITY_2, null, COUNTRY);

    public static final String EVENT_NAME = "Peter Broderick with Mesparrow at Ubu (March 9, 2017)";
    public static final String ANOTHER_EVENT_NAME = "Peter Broderick at Emmauskirche in Berlin, Germany";

    public static final String VENUE_NAME = "Ubu";
    public static final String VENUE_WEBSITE = "http://www.ubu-rennes.com";
    public static final String VENUE_PHONE = "0299311210";

    public static final String DATE_TIME = "2017-05-06T19:45:00";
    public static final String DATE = "2017-05-06";
    public static final String TIME = "19:45:00";

    public static final String ANOTHER_DATE_TIME = "2017-05-06T20:00:00";
    public static final String ANOTHER_TIME = "20:00:00";

    public static final StartTime START_TIME = new StartTime(DATE_TIME, DATE, TIME);
    public static final StartTime ANOTHER_START_TIME = new StartTime(ANOTHER_DATE_TIME, DATE, ANOTHER_TIME);

    public static final Venue VENUE =
            new Venue(VENUE_NAME, AREA, VENUE_WEBSITE, VENUE_PHONE, null, true);

    public static final Long SONGKICK_EVENT_ID = 29132634L;

    public static final List<Artist> ARTISTS = Collections.singletonList(ARTIST_PETER_BRODERICK);

    public static final Event EVENT =
            new Event(SONGKICK_EVENT_ID, null, EVENT_NAME, ARTISTS, VENUE, START_TIME);

    public static final String LANGUAGE_CODE = "en";

    public static final String USER_NAME = "lindabjalla";
}