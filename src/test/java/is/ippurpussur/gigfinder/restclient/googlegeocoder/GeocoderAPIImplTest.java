package is.ippurpussur.gigfinder.restclient.googlegeocoder;

import is.ippurpussur.gigfinder.model.parser.Key;
import org.junit.Test;

import java.util.Map;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class GeocoderAPIImplTest {

    @Test
    public void shouldGetCityAndCountry() throws Exception {
        final GeocoderAPIImpl geocoderService = new GeocoderAPIImpl();

        final Map<String, String> cityAndCountry = geocoderService.getAddress(LATITUDE, LONGITUDE, LANGUAGE_CODE);
        final String city = cityAndCountry != null ? cityAndCountry.get(Key.CITY) : null;
        final String country = cityAndCountry != null ? cityAndCountry.get(Key.COUNTRY) : null;

        final String languageCodeJapanese = "ja";
        final Map<String, String> cityAndCountryInJapanese = geocoderService.getAddress(LATITUDE, LONGITUDE, languageCodeJapanese);
        final String cityInJapanese = cityAndCountryInJapanese != null ? cityAndCountryInJapanese.get(Key.CITY) : null;
        final String countryInJapanese = cityAndCountryInJapanese != null ? cityAndCountryInJapanese.get(Key.COUNTRY) : null;

        final Map<String, String> anotherCityAndCountry = geocoderService.getAddress(41.0663668, 29.0151766, LANGUAGE_CODE);
        final String anotherCity = anotherCityAndCountry != null ? anotherCityAndCountry.get(Key.CITY) : null;
        final String anotherCountry = anotherCityAndCountry != null ? anotherCityAndCountry.get(Key.COUNTRY) : null;
        final String languageCodeTurkish = "tr";
        final Map<String, String> anotherCityAndCountryInTurkish =
                geocoderService.getAddress(41.0663668, 29.0151766, languageCodeTurkish);
        final String anotherCityInTurkish =
                anotherCityAndCountryInTurkish != null ? anotherCityAndCountryInTurkish.get(Key.CITY) : null;
        final String anotherCountryInTurkish =
                anotherCityAndCountryInTurkish != null ? anotherCityAndCountryInTurkish.get(Key.COUNTRY) : null;

        assertThat(city, equalTo("Rennes"));
        assertThat(country, equalTo("France"));
        assertThat(cityInJapanese, equalTo("レンヌ"));
        assertThat(countryInJapanese, equalTo("フランス"));
        assertThat(anotherCity, equalTo("Istanbul"));
        assertThat(anotherCountry, equalTo("Turkey"));
        assertThat(anotherCityInTurkish, equalTo("İstanbul"));
        assertThat(anotherCountryInTurkish, equalTo("Türkiye"));
    }
}