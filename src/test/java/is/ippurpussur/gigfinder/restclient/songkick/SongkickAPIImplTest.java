package is.ippurpussur.gigfinder.restclient.songkick;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.StartTime;
import is.ippurpussur.gigfinder.model.parser.SongkickParser;
import is.ippurpussur.gigfinder.model.songkick.*;
import org.hamcrest.core.Every;
import org.junit.Test;

import java.util.*;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public final class SongkickAPIImplTest {

    private SongkickAPIImpl songkickService = new SongkickAPIImpl();

    @Test
    public void shouldGetUpcomingEventsByMultipleArtists() throws Exception {
        final Set<SongkickEvent> upcomingEvents = songkickService.getUpcomingEventsByMultipleArtists(ARTISTS);
        final Map<Long, List<String>> songkickIdArtistsMap = new HashMap<>();
        final SongkickParser songkickParser = new SongkickParser();

        upcomingEvents.forEach(songkickEvent -> {
            final List<Artist> artists = songkickParser.convertPerformanceListToArtistList(songkickEvent.getPerformanceList());
            final List<String> artistNames = new ArrayList<>();
            artists.forEach(artist -> artistNames.add(artist.getName()));
            songkickIdArtistsMap.put(songkickEvent.getSongkickId(), artistNames);
        });

        final List<Artist> artists2 = Arrays.asList(ARTIST_PETER_BRODERICK, ARTIST_ALBUM_LEAF);
        final Set<SongkickEvent> upcomingEvents2 = songkickService.getUpcomingEventsByMultipleArtists(artists2);

        final List<Artist> artists3 = Arrays.asList(ARTIST_PETER_BRODERICK, ARTIST_ALBUM_LEAF, ARTIST_JOHAN_JOHANSSON);
        final Set<SongkickEvent> upcomingEvent3 = songkickService.getUpcomingEventsByMultipleArtists(artists3);

        assertThat(Lists.newArrayList(songkickIdArtistsMap.values()), Every.everyItem(hasItem(ARTIST_PETER_BRODERICK.getName())));
        assertThat(upcomingEvents2.size(), greaterThanOrEqualTo(upcomingEvents.size()));
        assertThat(upcomingEvent3.size(), greaterThanOrEqualTo(upcomingEvents2.size()));
    }

    @Test
    public void shouldSetVenueDetailsToEvents() throws Exception {
        final List<Identifier> identifier = Collections.singletonList(new Identifier(PETER_BRODERICK_MBID));
        final Performance performance = new Performance(new SongkickArtist(PETER_BRODERICK_NAME, identifier));
        final List<Performance> performanceList = Collections.singletonList(performance);

        final SongkickEvent songkickEvent = new SongkickEvent(
                29023889L,
                "Peter Broderick at Emmauskirche (May 6, 2017)",
                performanceList,
                new SongkickVenue(762011L, "Emmauskirche", 52.49993, 13.43093),
                new StartTime("2017-05-06T19:45:00", "2017-05-06", "19:45:00")
        );

        final SongkickEvent songkickEvent2 = new SongkickEvent(
                29051189L,
                "Peter Broderick with Mesparrow at Ubu (March 9, 2017)",
                performanceList,
                new SongkickVenue(33361L, VENUE_NAME, LATITUDE, LONGITUDE),
                new StartTime(DATE_TIME, DATE, TIME)
        );

        final List<SongkickEvent> songkickEvents = Arrays.asList(songkickEvent, songkickEvent2);
        final List<SongkickEvent> songkickEventsWithVenueDetails = songkickService.setVenueDetailsToEvents(songkickEvents);

        assertThat(songkickEventsWithVenueDetails.get(0).getVenue().getCity(), notNullValue());
        assertThat(songkickEventsWithVenueDetails.get(1).getVenue().getCity(), notNullValue());
    }
}
