package is.ippurpussur.gigfinder.restclient;

import org.junit.Test;

import java.util.Map;

public class envKeyTest {

    @Test
    public void shouldGetEnvKey () {
        Map<String, String> env = System.getenv();

        for (String envName : env.keySet()) {
            System.out.format("%s=%s%n",
                    envName,
                    env.get(envName));
        }
    }
}
