package is.ippurpussur.gigfinder.restclient.lastfm;

import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.MatchLevel;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public final class LastFmAPIImplTest {

    //TODO: någon gång i framtiden kolla på gherkin test

    private LastFmAPIImpl lastFmService = new LastFmAPIImpl();

    @Test
    public void shouldGetTopArtists() throws Exception {
        final List<Artist> topArtists = lastFmService.getTopArtistsByLastFmUsername(USER_NAME);
        final int defaultNumberOfResult = 50;

        assertThat(topArtists, hasSize(defaultNumberOfResult));
    }

    @Test
    public void shouldCreateMatchLevelsOfMultipleArtists() throws Exception {
        final int limit = 10;

        final Set<MatchLevel> matchLevelsOfMultipleArtists = lastFmService.createMatchLevelsOfMultipleArtists(ARTISTS, limit);

        final List<Artist> artists2 = Arrays.asList(ARTIST_PETER_BRODERICK, ARTIST_MUM);
        final Set<MatchLevel> matchLevelsOfMultipleArtists2 = lastFmService.createMatchLevelsOfMultipleArtists(artists2, limit);

        assertThat(matchLevelsOfMultipleArtists, hasSize(10));
        assertThat(matchLevelsOfMultipleArtists2, hasSize(20));
    }
}
