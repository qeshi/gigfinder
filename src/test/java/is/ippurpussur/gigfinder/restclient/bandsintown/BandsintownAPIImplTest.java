package is.ippurpussur.gigfinder.restclient.bandsintown;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.Area;
import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.Event;
import is.ippurpussur.gigfinder.neo4j.service.AreaService;
import org.hamcrest.core.Every;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BandsintownAPIImplTest {

    @Mock
    private AreaService areaServiceMock;

    private Area areaWithId;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(areaServiceMock);

        areaWithId = new Area(AREA);
        areaWithId.setId(1L);
    }

    @Test
    public void shouldGetUpcomingEventsByMultipleArtists() throws Exception {
        when(areaServiceMock.findByFormattedCoordinates(FORMATTED_LATITUDE, FORMATTED_LONGITUDE))
                .thenReturn(Collections.singletonList(areaWithId));

        final BandsintownAPIImpl bandsintownService = new BandsintownAPIImpl();
        final List<Artist> artists = Collections.singletonList(ARTIST_PETER_BRODERICK);

        final Set<Event> upcomingEvents =
                bandsintownService.getUpcomingEventsByMultipleArtists(artists, areaServiceMock, LANGUAGE_CODE);
        final Map<Long, List<String>> eventArtists = new HashMap<>();
        upcomingEvents.forEach(event -> {
            List<String> artistNames = new ArrayList<>();
            event.getArtists().forEach(artist -> artistNames.add(artist.getName()));
            eventArtists.put(event.getBandsintownId(), artistNames);
        });

        final List<Artist> artists2 = Arrays.asList(ARTIST_PETER_BRODERICK, ARTIST_ALBUM_LEAF);
        final Set<Event> upcomingEvents2 =
                bandsintownService.getUpcomingEventsByMultipleArtists(artists2, areaServiceMock, LANGUAGE_CODE);

        final List<Artist> artists3 = Arrays.asList(ARTIST_PETER_BRODERICK, ARTIST_ALBUM_LEAF, ARTIST_JOHAN_JOHANSSON);
        final Set<Event> upcomingEvents3 =
                bandsintownService.getUpcomingEventsByMultipleArtists(artists3, areaServiceMock, LANGUAGE_CODE);

        assertThat(Lists.newArrayList(eventArtists.values()), Every.everyItem(hasItem(ARTIST_PETER_BRODERICK.getName())));
        assertThat(upcomingEvents2.size(), greaterThanOrEqualTo(upcomingEvents.size()));
        assertThat(upcomingEvents3.size(), greaterThanOrEqualTo(upcomingEvents2.size()));
    }
}