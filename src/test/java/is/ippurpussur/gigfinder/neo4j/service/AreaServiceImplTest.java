package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Area;
import is.ippurpussur.gigfinder.model.neo4j.Coordinates;
import is.ippurpussur.gigfinder.neo4j.repository.AreaRepository;
import is.ippurpussur.gigfinder.neo4j.repository.CoordinatesRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AreaServiceImplTest extends AbstractServiceImplTest<Area, AreaRepository, AreaServiceImpl> {

    @InjectMocks
    private AreaServiceImpl areaService;

    @Mock
    private AreaRepository areaRepositoryMock;

    @Mock
    private CoordinatesRepository coordinatesRepositoryMock;

    private Area areaWithId;

    private Area areaWithId2;

    private Coordinates coordinatesWithId;

    private List<Area> areas;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(AreaRepository.class);
        MockitoAnnotations.initMocks(CoordinatesRepository.class);

        coordinatesWithId = new Coordinates(COORDINATES);
        coordinatesWithId.setId(3L);
        coordinatesWithId.setUuid("3");

        areaWithId = new Area(AREA);
        areaWithId.setId(1L);
        areaWithId.setUuid("1");
        areaWithId.setCoordinates(coordinatesWithId);

        areaWithId2 = new Area(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP);
        areaWithId2.setId(2L);
        areaWithId2.setUuid("2");

        areas = new ArrayList<>(Arrays.asList(areaWithId, areaWithId2));
    }

    @Test
    public void shouldCreate() throws Exception {
        final Area areaWithCoordinatesWithId = new Area(AREA);
        areaWithCoordinatesWithId.setCoordinates(coordinatesWithId);

        final Coordinates coordinatesWithId2 = new Coordinates(COORDINATES_2);
        coordinatesWithId2.setId(4L);
        coordinatesWithId2.setUuid("4");

        final Area anotherAreaWithExistingId = new Area(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP);
        anotherAreaWithExistingId.setId(1L);
        anotherAreaWithExistingId.setUuid("1");

        when(areaRepositoryMock.create(STREET, ZIP, CITY, null, COUNTRY, AREA.getLatLng(), AREA.getAddress()))
                .thenReturn(areaWithId);
        when(areaRepositoryMock.merge("1", STREET, ZIP, CITY, null, COUNTRY, AREA.getLatLng(), AREA.getAddress()))
                .thenReturn(areaWithId);
        when(areaRepositoryMock.updateRelationshipWithCoordinates(areaWithId.getLatLng()))
                .thenReturn(areaWithId);
        when(areaRepositoryMock.findOne(areaWithId.getId())).thenReturn(areaWithId);

        when(areaRepositoryMock.create(
                STREET,
                null,
                CITY_2,
                null,
                COUNTRY,
                COORDINATES_2.getLatLng(),
                AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP.getAddress()))
                .thenReturn(areaWithId2);

        when(areaRepositoryMock.merge(
                "2",
                STREET,
                null,
                CITY_2,
                null,
                COUNTRY,
                COORDINATES_2.getLatLng(),
                AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP.getAddress()))
                .thenReturn(areaWithId2);

        when(areaRepositoryMock.updateRelationshipWithCoordinates(areaWithId2.getLatLng()))
                .thenReturn(areaWithId);
        when(areaRepositoryMock.findOne(areaWithId2.getId())).thenReturn(areaWithId2);

        when(areaRepositoryMock.merge(
                "1",
                STREET,
                null,
                CITY_2,
                null,
                COUNTRY,
                COORDINATES_2.getLatLng(),
                AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP.getAddress()))
                .thenReturn(anotherAreaWithExistingId);

        when(areaRepositoryMock.updateRelationshipWithCoordinates(anotherAreaWithExistingId.getLatLng()))
                .thenReturn(anotherAreaWithExistingId);
        when(areaRepositoryMock.findByLatLng(areaWithId2.getLatLng())).thenReturn(areaWithId2);
        when(coordinatesRepositoryMock.findByFormattedLatitudeAndFormattedLongitude(FORMATTED_LATITUDE, FORMATTED_LONGITUDE))
                .thenReturn(null);

        final Area area = areaService.create(AREA);
        final Area area2 = areaService.create(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP);

        when(areaRepositoryMock.findByLatLng(anotherAreaWithExistingId.getLatLng())).thenReturn(anotherAreaWithExistingId);
        when(areaRepositoryMock.findOne(anotherAreaWithExistingId.getId())).thenReturn(anotherAreaWithExistingId);

        final Area area3 = areaService.create(anotherAreaWithExistingId);

        when(areaRepositoryMock.updateRelationshipWithCoordinates(areaWithId.getLatLng()))
                .thenReturn(areaWithId);
        when(areaRepositoryMock.findOne(areaWithId.getId())).thenReturn(areaWithId);

        final Area area4 = areaService.create(AREA);

        assertThat(area, equalTo(areaWithId));
        assertThat(area2, equalTo(areaWithId2));
        assertThat(area2.getId(), not(equalTo(area3.getId())));
        assertThat(area3, equalTo(anotherAreaWithExistingId));
        assertThat(area3.getId(), equalTo(area.getId()));
        assertThat(area4, equalTo(area));
    }

    @Test
    public void shouldDelete() throws Exception {
        shouldDelete(areas, areaWithId, areaWithId2, areaRepositoryMock, areaService);
    }

    @Test
    public void shouldFindById() throws Exception {
        shouldFindById(areaRepositoryMock, areaWithId, areaService);
    }

    @Test
    public void shouldFindAll() throws Exception {
        shouldFindAll(areaRepositoryMock, areas, areaService);
    }

    @Test
    public void shouldDeleteAll() throws Exception {
        shouldDeleteAll(areas, areaRepositoryMock, areaService);
    }

    @Test
    public void shouldFindByFormattedCoordinates() throws Exception {
        final List<Area> areaList = Collections.singletonList(areaWithId);
        when(areaRepositoryMock.findByCoordinatesFormattedLatitudeAndCoordinatesFormattedLongitude(
                FORMATTED_LATITUDE, FORMATTED_LONGITUDE)).thenReturn(areaList);

        final List<Area> areasByFormattedCoordinates =
                areaService.findByFormattedCoordinates(FORMATTED_LATITUDE, FORMATTED_LONGITUDE);

        assertThat(areasByFormattedCoordinates, hasSize(1));
        assertThat(areasByFormattedCoordinates, hasItem(areaWithId));
    }
}
