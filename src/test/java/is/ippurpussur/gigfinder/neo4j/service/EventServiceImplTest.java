package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.*;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventServiceImplTest extends AbstractServiceImplTest<Event, EventRepository, EventServiceImpl> {

    @InjectMocks
    private EventServiceImpl eventService;

    @Mock
    private EventRepository eventRepositoryMock;

    @Mock
    private ArtistRepository artistRepositoryMock;

    @Mock
    private CoordinatesRepository coordinatesRepositoryMock;

    @Mock
    private AreaRepository areaRepositoryMock;

    @Mock
    private VenueRepository venueRepositoryMock;

    @Mock
    private StartTimeRepository startTimeRepositoryMock;

    private Event event;

    private Event eventWithId;

    private Event eventWithId2;

    private Event sameEventWithDifferentId;

    private List<Event> events;

    private List<Event> eventsWithId;

    private List<Event> singletonEventList;

    private int pageNumber1 = 1;

    private int pageNumber2 = 2;

    private int numberOfResultsPerPage = 2;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(eventRepositoryMock);
        MockitoAnnotations.initMocks(artistRepositoryMock);
        MockitoAnnotations.initMocks(coordinatesRepositoryMock);
        MockitoAnnotations.initMocks(areaRepositoryMock);
        MockitoAnnotations.initMocks(venueRepositoryMock);
        MockitoAnnotations.initMocks(startTimeRepositoryMock);

        sameEventWithDifferentId = new Event(EVENT);
        sameEventWithDifferentId.setId(3L);

        final Artist artistWithId = new Artist(ARTIST_PETER_BRODERICK);
        artistWithId.setId(4L);
        artistWithId.setUuid("4");

        final Artist artistWithId2 = new Artist(ARTIST_MUM);
        artistWithId2.setId(5L);
        artistWithId2.setUuid("5");

        final Coordinates coordinatesWithId = new Coordinates(COORDINATES);
        coordinatesWithId.setId(6L);

        final Area areaWithId = new Area(AREA);
        areaWithId.setId(7L);
        areaWithId.setUuid("7");

        final Venue venueWithId = new Venue(VENUE);
        venueWithId.setId(8L);
        venueWithId.setUuid("8");

        final StartTime startTimeWithId = new StartTime(DATE_TIME, DATE, TIME);
        startTimeWithId.setId(9L);
        startTimeWithId.setUuid("9");

        final List<Area> areas = Collections.singletonList(areaWithId);
        final List<Venue> venues = Collections.singletonList(venueWithId);
        final List<StartTime> startTimes = Collections.singletonList(startTimeWithId);
        final List<Artist> artists = Collections.singletonList(artistWithId);
        final List<Artist> artists2 = Collections.singletonList(artistWithId2);

        event = new Event(SONGKICK_EVENT_ID, null, EVENT_NAME, ARTISTS, VENUE, startTimeWithId);

        eventWithId = new Event(EVENT);
        eventWithId.setId(1L);
        eventWithId.setUuid("1");
        eventWithId.setStartTime(startTimeWithId);
        eventWithId.setArtists(artists);
        eventWithId.setVenue(venueWithId);

        final Event anotherEvent = new Event(eventWithId);
        anotherEvent.setId(null);
        anotherEvent.setSongkickId(10L);
        anotherEvent.setName("múm event");
        anotherEvent.setArtists(artists2);

        eventWithId2 = new Event(anotherEvent);
        eventWithId2.setId(2L);
        eventWithId2.setUuid("2");

        events = Arrays.asList(EVENT, anotherEvent);
        eventsWithId = new ArrayList<>(Arrays.asList(eventWithId, eventWithId2));
        singletonEventList = Collections.singletonList(eventWithId);

        when(eventRepositoryMock.create(EVENT_NAME, SONGKICK_EVENT_ID, null)).thenReturn(eventWithId);
        when(eventRepositoryMock.merge("1", SONGKICK_EVENT_ID, null, EVENT_NAME))
                .thenReturn(eventWithId);
        when(eventRepositoryMock.merge("3", SONGKICK_EVENT_ID, null, EVENT_NAME))
                .thenReturn(sameEventWithDifferentId);
        when(eventRepositoryMock.findBySongkickId(SONGKICK_EVENT_ID)).thenReturn(eventWithId);
        when(eventRepositoryMock.findOne(1L)).thenReturn(eventWithId);

        when(artistRepositoryMock.findByMbid(PETER_BRODERICK_MBID)).thenReturn(artistWithId);
        when(artistRepositoryMock.merge("4", PETER_BRODERICK_NAME, PETER_BRODERICK_MBID, null))
                .thenReturn(artistWithId);
        when(artistRepositoryMock.findByMbid(MUM_MBID)).thenReturn(artistWithId2);
        when(artistRepositoryMock.merge("5", MUM_NAME, MUM_MBID, null))
                .thenReturn(artistWithId2);

        when(coordinatesRepositoryMock.findByFormattedLatitudeAndFormattedLongitude(FORMATTED_LATITUDE, FORMATTED_LONGITUDE))
                .thenReturn(coordinatesWithId);

        when(areaRepositoryMock.findByCoordinatesFormattedLatitudeAndCoordinatesFormattedLongitude(
                FORMATTED_LATITUDE, FORMATTED_LONGITUDE)).thenReturn(areas);
        when(areaRepositoryMock.create(STREET, ZIP, CITY, null, COUNTRY, areaWithId.getLatLng(), areaWithId.getAddress()))
                .thenReturn(areaWithId);

        when(venueRepositoryMock.findByformattedCoordinates(FORMATTED_LATITUDE, FORMATTED_LONGITUDE)).thenReturn(venues);
        when(venueRepositoryMock.create(VENUE_NAME, VENUE_WEBSITE, VENUE_PHONE, null, venueWithId.getLatLng()))
                .thenReturn(venueWithId);
        when(venueRepositoryMock
                .merge("8", VENUE_NAME, VENUE_WEBSITE, VENUE_PHONE, null, venueWithId.getLatLng()))
                .thenReturn(venueWithId);

        when(startTimeRepositoryMock.findByDateTime(DATE_TIME)).thenReturn(startTimeWithId);
        when(startTimeRepositoryMock.findByDate(DATE)).thenReturn(startTimes);
        when(startTimeRepositoryMock.merge("9", DATE_TIME, DATE, TIME)).thenReturn(startTimeWithId);
    }

    @Test
    public void shouldCreate() throws Exception {
        final Event event = eventService.create(this.event);
        final Event event2 = eventService.create(this.event);
        final Event event3 = eventService.create(eventWithId);
        final Event event4 = eventService.create(sameEventWithDifferentId);

        assertThat(event, equalTo(eventWithId));
        assertThat(event2, equalTo(event));
        assertThat(event3, equalTo(event));
        assertThat(event4, equalTo(event));
    }

    @Test
    public void shouldDelete() throws Exception {
        shouldDelete(eventsWithId, eventWithId, eventWithId2, eventRepositoryMock, eventService);
    }

    @Test
    public void shouldFindById() throws Exception {
        shouldFindById(eventRepositoryMock, eventWithId, eventService);
    }

    @Test
    public void shouldFindAll() throws Exception {
        shouldFindAll(eventRepositoryMock, eventsWithId, eventService);
    }

    @Test
    public void shouldFindAllSortByStartDateTime() {
        when(eventRepositoryMock.findAllSortByStartDateTime(0, 2)).thenReturn(events);
        when(eventRepositoryMock.findAllSortByStartDateTime(2, 2)).thenReturn(new ArrayList<>());

        final List<Event> events1 = eventService.findAllSortByStartDateTime(pageNumber1, numberOfResultsPerPage);
        final List<Event> events2 = eventService.findAllSortByStartDateTime(pageNumber2, numberOfResultsPerPage);

        assertThat(events1, equalTo(events));
        assertThat(events2, hasSize(0));
    }

    @Test
    public void shouldCreateMany() throws Exception {
        final String eventName = "múm event";
        when(eventRepositoryMock.create(eventName, SONGKICK_EVENT_ID, null))
                .thenReturn(eventWithId2);
        when(eventRepositoryMock.merge("2", 10L, null, eventName))
                .thenReturn(eventWithId2);
        when(eventRepositoryMock.findOne(1L, 3)).thenReturn(eventWithId);
        when(eventRepositoryMock.findOne(2L, 3)).thenReturn(eventWithId2);
        when(eventRepositoryMock.findOne(2L)).thenReturn(eventWithId2);
        when(eventRepositoryMock.findBySongkickId(10L)).thenReturn(eventWithId2);

        final List<Event> createdEvents = eventService.createMany(events);

        assertThat(createdEvents, hasSize(2));
        assertThat(createdEvents, hasItems(eventWithId, eventWithId2));
    }

    @Test
    public void shouldDeleteAll() throws Exception {
        shouldDeleteAll(eventsWithId, eventRepositoryMock, eventService);
    }

    @Test
    public void shouldFindByName() throws Exception {
        when(eventRepositoryMock.findByName(EVENT_NAME)).thenReturn(singletonEventList);

        final List<Event> eventsByName = eventService.findByName(EVENT_NAME);

        assertThat(eventsByName, hasSize(1));
        assertThat(eventsByName, equalTo(singletonEventList));
    }

    @Test
    public void shouldFindByNameLike() throws Exception {
        when(eventRepositoryMock.findByNameLike("Peter Broderick")).thenReturn(singletonEventList);

        final List<Event> eventsByNameLike = eventService.findByNameLike("Peter Broderick");

        assertThat(eventsByNameLike, hasSize(1));
        assertThat(eventsByNameLike, equalTo(singletonEventList));
    }

    @Test
    public void shouldFindByStartDateAndTime() throws Exception {
        when(eventRepositoryMock.findByStartTimeDateAndStartTimeTime(DATE, TIME)).thenReturn(eventsWithId);

        final List<Event> eventsByStartDateAndTime =
                eventService.findByStartDateAndStartTime(DATE, TIME);

        assertThat(eventsByStartDateAndTime, hasSize(2));
        assertThat(eventsByStartDateAndTime, equalTo(eventsWithId));
    }

    @Test
    public void shouldFindByArtistName() throws Exception {
        when(eventRepositoryMock.findByArtistName(PETER_BRODERICK_NAME)).thenReturn(singletonEventList);

        final List<Event> events = eventService.findByArtistName(PETER_BRODERICK_NAME);

        assertThat(events, hasSize(1));
        assertThat(events, equalTo(singletonEventList));
    }

    @Test
    public void shouldFindByStartDate() throws Exception {
        when(eventRepositoryMock.findByStartTimeDate(DATE)).thenReturn(eventsWithId);

        final List<Event> eventsByStartDate = eventService.findByStartDate(DATE);

        assertThat(eventsByStartDate, hasSize(2));
        assertThat(eventsByStartDate, equalTo(eventsWithId));
    }

    @Test
    public void shouldFindByCity() {
        final Venue venueInParis = new Venue(VENUE);
        venueInParis.setArea(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP);

        final Event eventInParis = new Event(EVENT);
        eventInParis.setVenue(venueInParis);

        final List<Event> eventsInParis = Collections.singletonList(eventInParis);

        when(eventRepositoryMock.findByCity(CITY, 0, 2)).thenReturn(singletonEventList);
        when(eventRepositoryMock.findByCity(CITY, 2, 2)).thenReturn(new ArrayList<>());
        when(eventRepositoryMock.findByCity(CITY_2, 0, 2)).thenReturn(eventsInParis);

        final List<Event> events1 = eventService.findByCity(CITY, pageNumber1, numberOfResultsPerPage);
        final List<Event> events2 = eventService.findByCity(CITY, pageNumber2, numberOfResultsPerPage);
        final List<Event> events3 = eventService.findByCity(CITY_2, pageNumber1, numberOfResultsPerPage);

        assertThat(events1, equalTo(singletonEventList));
        assertThat(events2, hasSize(0));
        assertThat(events3, equalTo(eventsInParis));
    }
}
