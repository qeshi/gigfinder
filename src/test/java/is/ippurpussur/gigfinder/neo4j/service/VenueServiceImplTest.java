package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Area;
import is.ippurpussur.gigfinder.model.neo4j.Venue;
import is.ippurpussur.gigfinder.neo4j.repository.AreaRepository;
import is.ippurpussur.gigfinder.neo4j.repository.CoordinatesRepository;
import is.ippurpussur.gigfinder.neo4j.repository.VenueRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VenueServiceImplTest extends AbstractServiceImplTest<Venue, VenueRepository, VenueServiceImpl> {

    @InjectMocks
    private VenueServiceImpl venueService;

    @Mock
    private VenueRepository venueRepositoryMock;

    @Mock
    private CoordinatesRepository coordinatesRepositoryMock;

    @Mock
    private AreaRepository areaRepositoryMock;

    private Venue venue;

    private Venue venue2;

    private Venue venueWithId;

    private Venue venueWithId2;

    private Venue venueWithExistingId;

    private List<Venue> venues;

    private Area area;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(venueRepositoryMock);
        MockitoAnnotations.initMocks(coordinatesRepositoryMock);
        MockitoAnnotations.initMocks(areaRepositoryMock);

        area = new Area(AREA);
        area.setId(3L);
        area.setUuid("3");

        Area area2 = new Area(AREA_WITH_OTHER_COORDINATES_AND_NULL_ZIP);
        area2.setId(4L);
        area2.setUuid("4");

        venue = new Venue(VENUE);
        venue.setArea(area);

        venue2 = new Venue(VENUE);
        venue2.setName("Cafe Oto");
        venue2.setArea(area2);

        venueWithId = new Venue(VENUE);
        venueWithId.setId(1L);
        venueWithId.setUuid("1");
        venueWithId.setArea(area);

        venueWithId2 = new Venue(venue2);
        venueWithId2.setId(2L);
        venueWithId2.setUuid("2");
        venueWithId2.setArea(area2);

        venueWithExistingId = new Venue(venue2);
        venueWithExistingId.setId(1L);
        venueWithExistingId.setUuid("1");

        venues = new ArrayList<>(Arrays.asList(venueWithId, venueWithId2));
    }

    //TODO: dela upp testerna
    @Test
    public void shouldCreate() throws Exception {
        when(venueRepositoryMock.create(VENUE_NAME, VENUE_WEBSITE, VENUE_PHONE, null, venueWithId.getLatLng()))
                .thenReturn(venueWithId);

        when(venueRepositoryMock.create(
                venue2.getName(), venue2.getWebsite(), venue2.getPhone(), venue2.getDescription(), venue2.getLatLng()))
                .thenReturn(venueWithId2);

        when(venueRepositoryMock
                .merge("1", VENUE_NAME, VENUE_WEBSITE, VENUE_PHONE, null, venueWithId.getLatLng()))
                .thenReturn(venueWithId);

        when(venueRepositoryMock
                .merge("2", venue2.getName(), venue2.getWebsite(), venue2.getPhone(), venue2.getDescription(), venue2.getLatLng()))
                .thenReturn(venueWithId2);

        when(venueRepositoryMock
                .merge(
                        "1",
                        venueWithExistingId.getName(),
                        venueWithExistingId.getWebsite(),
                        venueWithExistingId.getPhone(),
                        venueWithExistingId.getDescription(),
                        venueWithExistingId.getLatLng()))
                .thenReturn(venueWithExistingId);

        when(coordinatesRepositoryMock.findByFormattedLatitudeAndFormattedLongitude(FORMATTED_LATITUDE, FORMATTED_LONGITUDE))
                .thenReturn(COORDINATES);
        when(areaRepositoryMock.findByCoordinatesFormattedLatitudeAndCoordinatesFormattedLongitude(FORMATTED_LATITUDE, FORMATTED_LONGITUDE))
                .thenReturn(Collections.singletonList(area));

        when(areaRepositoryMock.findByLatLng(venueWithId.getLatLng())).thenReturn(venueWithId.getArea());
        when(areaRepositoryMock.findByLatLng(venueWithId2.getLatLng())).thenReturn(venueWithId2.getArea());

        when(venueRepositoryMock.findByLatLng(venueWithId.getLatLng())).thenReturn(venueWithId);
        when(venueRepositoryMock.findByLatLng(venueWithId2.getLatLng())).thenReturn(venueWithId2);

        when(venueRepositoryMock.findOne(1L, 3)).thenReturn(venueWithId);
        when(venueRepositoryMock.findOne(2L, 3)).thenReturn(venueWithId2);

        when(venueRepositoryMock.findByUuid("1")).thenReturn(venueWithId);
        when(venueRepositoryMock.updateRelationshipWithArea("1")).thenReturn(venueWithId);
        when(venueRepositoryMock.findByUuid("2")).thenReturn(venueWithId2);
        when(venueRepositoryMock.updateRelationshipWithArea("2")).thenReturn(venueWithId2);

        final Venue createdVenue = venueService.create(venue);
        final Venue createdVenue2 = venueService.create(venue);
        final Venue createdVenue3 = venueService.create(venue2);

        when(venueRepositoryMock.findByUuid("1")).thenReturn(venueWithExistingId);
        when(venueRepositoryMock.findOne(1L, 3)).thenReturn(venueWithExistingId);

        final Venue createdVenue4 = venueService.create(venueWithExistingId);

        assertThat(createdVenue, equalTo(venueWithId));
        assertThat(createdVenue2, equalTo(createdVenue));
        assertThat(createdVenue3, not(equalTo(createdVenue)));
        assertThat(createdVenue4, equalTo(venueWithExistingId));
    }

    @Test
    public void shouldDelete() throws Exception {
        shouldDelete(venues, venueWithId, venueWithId2, venueRepositoryMock, venueService);
    }

    @Test
    public void shouldFindById() throws Exception {
        shouldFindById(venueRepositoryMock, venueWithId, venueService);
    }

    @Test
    public void shouldFindAll() throws Exception {
        shouldFindAll(venueRepositoryMock, venues, venueService);
    }

    @Test
    public void shouldDeleteAll() throws Exception {
        shouldDeleteAll(venues, venueRepositoryMock, venueService);
    }

    @Test
    public void shouldFindByName() throws Exception {
        when(venueRepositoryMock.findByName(VENUE_NAME)).thenReturn(venueWithId);
        when(venueRepositoryMock.findOne(venueWithId.getId(), 2)).thenReturn(venueWithId);

        final Venue venueByName = venueService.findByName(VENUE_NAME);

        assertThat(venueByName, equalTo(venueWithId));
    }

    @Test
    public void shouldFindByNameAndCity() throws Exception {
        when(venueRepositoryMock.findByNameAndCity(VENUE_NAME, CITY)).thenReturn(venueWithId);

        final Venue venueByName = venueService.findByNameAndCity(VENUE_NAME, CITY);

        assertThat(venueByName, equalTo(venueWithId));
    }

    @Test
    public void shouldFindByFormattedCoordinates() throws Exception {
        when(venueRepositoryMock.findByformattedCoordinates(FORMATTED_LATITUDE, FORMATTED_LONGITUDE))
                .thenReturn(Collections.singletonList(venueWithId));

        final List<Venue> venuesByFormattedCoordinates = venueService.findByFormattedCoordinates(FORMATTED_LATITUDE, FORMATTED_LONGITUDE);

        assertThat(venuesByFormattedCoordinates, hasSize(1));
        assertThat(venuesByFormattedCoordinates, hasItem(venueWithId));
    }

    @Test
    public void shouldFindByCoordinatesBetween() throws Exception {
        final Double minLat = FORMATTED_LATITUDE - 5;
        final Double maxLat = FORMATTED_LATITUDE + 5;
        final Double minLng = FORMATTED_LONGITUDE - 5;
        final Double maxLng = FORMATTED_LONGITUDE + 5;

        when(venueRepositoryMock.findByCoordinatesBetween(minLat, maxLat, minLng, maxLng)).thenReturn(venues);

        final List<Venue> venuesByCoordinatesBetween = venueService.findByCoordinatesBetween(minLat, maxLat, minLng, maxLng);

        assertThat(venuesByCoordinatesBetween, hasSize(2));
        assertThat(venuesByCoordinatesBetween, hasItems(venueWithId, venueWithId2));
    }
}