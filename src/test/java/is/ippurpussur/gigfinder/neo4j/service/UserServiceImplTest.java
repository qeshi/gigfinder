package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.User;
import is.ippurpussur.gigfinder.neo4j.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.USER_NAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest extends AbstractServiceImplTest<User, UserRepository, UserServiceImpl> {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepositoryMock;

    private User user;

    private User user2;

    private User userWithId;

    private User userWithId2;

    private User userWithExistingId;

    private List<User> users;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(userRepositoryMock);

        user = new User(USER_NAME);
        user2 = new User("izumi");

        userWithId = new User(user);
        userWithId.setId(1L);
        userWithId.setUuid("1");

        userWithId2 = new User(user2);
        userWithId2.setId(2L);
        userWithId2.setUuid("2");

        userWithExistingId = new User(user2);
        userWithExistingId.setId(1L);
        userWithExistingId.setUuid("1");

        users = new ArrayList<>(Arrays.asList(userWithId, userWithId2));
    }

    @Test
    public void shouldCreate() throws Exception {
        when(userRepositoryMock.merge(userWithId.getUuid(), userWithId.getName())).thenReturn(userWithId);
        when(userRepositoryMock.merge(userWithId2.getUuid(), userWithId2.getName())).thenReturn(userWithId2);
        when(userRepositoryMock.merge(userWithExistingId.getUuid(), userWithExistingId.getName()))
                .thenReturn(userWithExistingId);
        when(userRepositoryMock.findByName(user.getName())).thenReturn(userWithId);
        when(userRepositoryMock.findByName(user2.getName())).thenReturn(userWithId2);
        when(userRepositoryMock.findOne(1L)).thenReturn(userWithId);
        when(userRepositoryMock.findOne(2L)).thenReturn(userWithId2);

        final User createdUser = userService.create(user);
        final User createdUser2 = userService.create(user);
        final User createdUser3 = userService.create(user2);
        final User createdUser4 = userService.create(userWithExistingId);

        assertThat(createdUser, equalTo(userWithId));
        assertThat(createdUser2, equalTo(createdUser));
        assertThat(createdUser3, not(equalTo(userWithId)));
        assertThat(createdUser4, equalTo(userWithId2));
    }

    @Test
    public void shouldDelete() throws Exception {
        shouldDelete(users, userWithId, userWithId2, userRepositoryMock, userService);
    }

    @Test
    public void shouldFindById() throws Exception {
        shouldFindById(userRepositoryMock, userWithId, userService);
    }

    @Test
    public void shouldFindAll() throws Exception {
        shouldFindAll(userRepositoryMock, users, userService);
    }

    @Test
    public void shouldDeleteAll() throws Exception {
        shouldDeleteAll(users, userRepositoryMock, userService);
    }

    @Test
    public void shouldFindByName() throws Exception {
        when(userRepositoryMock.findByName(USER_NAME)).thenReturn(userWithId);

        final User userByName = userService.findByName(USER_NAME);

        assertThat(userByName, equalTo(userWithId));
    }
}