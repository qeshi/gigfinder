package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.AbstractEntity;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

abstract class AbstractServiceImplTest<T extends AbstractEntity, R extends GraphRepository<T>, S extends GenericService<T>> {

    void shouldDelete(final List<T> objects, final T objectToDelete, final T objectToRemain, final R repositoryMock, final S service) {
        doAnswer(invocationOnMock -> objects.remove(objectToDelete)).when(repositoryMock).delete(objectToDelete);

        service.delete(objectToDelete);

        verify(repositoryMock).delete(objectToDelete);
        assertThat(objects, hasSize(1));
        assertThat(objects, hasItem(objectToRemain));
    }

    void shouldFindById(final R repositoryMock, final T object, final S service) {
        when(repositoryMock.findOne(object.getId(), 2)).thenReturn(object);

        final T objectById = service.findById(object.getId(), 2);

        assertThat(objectById, equalTo(object));
    }

    void shouldFindAll(final R repositoryMock, final List<T> objects, final S service) {
        when(repositoryMock.findAll()).thenReturn(objects);

        final List<T> allObjects = service.findAll();

        assertThat(allObjects, hasSize(2));
        assertThat(allObjects, hasItem(objects.get(0)));
        assertThat(allObjects, hasItem(objects.get(1)));
    }

    void shouldDeleteAll(final List<T> objects, final R repositoryMock, final S service) {
        doAnswer(invocationOnMock -> {
            objects.clear();
            return null;
        }).when(repositoryMock).deleteAll();

        service.deleteAll();

        verify(repositoryMock).deleteAll();
        assertThat(objects, is(empty()));
    }
}
