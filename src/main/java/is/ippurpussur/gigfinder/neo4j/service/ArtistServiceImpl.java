package is.ippurpussur.gigfinder.neo4j.service;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ArtistService")
public final class ArtistServiceImpl extends CommonService implements ArtistService {

    @Autowired
    public ArtistServiceImpl(final VenueRepository venueRepository,
                             final StartTimeRepository startTimeRepository,
                             final AreaRepository areaRepository,
                             final CoordinatesRepository coordinatesRepository,
                             final ArtistRepository artistRepository,
                             final EventRepository eventRepository) {
        super(venueRepository, startTimeRepository, areaRepository, coordinatesRepository, artistRepository, eventRepository);
    }

    @Override
    public Artist create(final Artist artist) {
        return createArtist(artist);
    }

    @Override
    public Artist merge(Artist artist) {
        return mergeArtist(artist);
    }

    @Override
    public void delete(final Artist artist) {
        artistRepository.delete(artist);
    }

    @Override
    public Artist findById(final Long id, final int depth) {
        return artistRepository.findOne(id, depth);
    }

    @Override
    public List<Artist> findAll() {
        final Iterable<Artist> artists = artistRepository.findAll();
        return Lists.newArrayList(artists);
    }

    @Override
    public Artist findByMbid(final String mbid) {
        return findArtistByMbid(mbid);
    }

    @Override
    public void deleteAll() {
        artistRepository.deleteAll();
    }

    @Override
    public List<Artist> findByName(final String name) {
        return findArtistsByArtistName(name);
    }

    @Override
    public List<Artist> createMany(final List<Artist> artists) {
        return createArtists(artists);
    }

    @Override
    public void createConstraint() {
        areaRepository.createConstraint();
    }
}
