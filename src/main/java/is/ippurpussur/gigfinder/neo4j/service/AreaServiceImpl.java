package is.ippurpussur.gigfinder.neo4j.service;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.Area;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service("AreaService")
public final class AreaServiceImpl extends CommonService implements AreaService {

    public AreaServiceImpl(final VenueRepository venueRepository,
                           final StartTimeRepository startTimeRepository,
                           final AreaRepository areaRepository,
                           final CoordinatesRepository coordinatesRepository,
                           final ArtistRepository artistRepository,
                           final EventRepository eventRepository) {
        super(venueRepository, startTimeRepository, areaRepository, coordinatesRepository, artistRepository, eventRepository);
    }

    @Override
    public Area create(final Area area) {
        return createArea(area);
    }

    @Override
    public Area merge(final Area area) {
        return mergeArea(area);
    }

    @Override
    public void delete(final Area area) {
        areaRepository.delete(area);
    }

    @Override
    public Area findById(final Long id, final int depth) {
        return areaRepository.findOne(id, depth);
    }

    @Override
    public List<Area> findAll() {
        final Iterable<Area> areas = areaRepository.findAll();
        return Lists.newArrayList(areas);
    }

    @Override
    public void deleteAll() {
        areaRepository.deleteAll();
    }

    @Override
    public List<Area> findByFormattedCoordinates(final Double formattedLatitude, final Double formattedLongitude) {
        return findAreasByFormattedCoordinates(formattedLatitude, formattedLongitude);
    }

    @Override
    public List<Area> findByFormattedCoordinatesBetween(
            final Double formattedLatitude, final Double formattedLongitude) {
        final Double errorRange = 0.1;

        final Double minLat = formattedLatitude - errorRange;
        final Double maxLat = formattedLatitude + errorRange;
        final Double minLng = formattedLatitude - errorRange;
        final Double maxLng = formattedLongitude + errorRange;

        final Collection<Area> areas = areaRepository.findByCoordinatesBetween(minLat, maxLat, minLng, maxLng);

        return Lists.newArrayList(areas);
    }

    @Override
    public Area findByLatLng(final String latLng) {
        return findAreaByLatLng(latLng);
    }

    @Override
    public void createConstraint() {
        areaRepository.createConstraint();
    }
}
