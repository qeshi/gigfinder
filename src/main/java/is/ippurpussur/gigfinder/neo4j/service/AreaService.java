package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Area;

import java.util.List;

public interface AreaService extends GenericService<Area> {

    List<Area> findByFormattedCoordinates(final Double formattedLatitude, final Double formattedLongitude);

    List<Area> findByFormattedCoordinatesBetween(final Double formattedLatitude, final Double formattedLongitude);

    Area findByLatLng(final String latLng);

    void createConstraint();
}
