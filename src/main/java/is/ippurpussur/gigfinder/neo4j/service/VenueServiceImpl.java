package is.ippurpussur.gigfinder.neo4j.service;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.Venue;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("VenueService")
public final class VenueServiceImpl extends CommonService implements VenueService {

    @Autowired
    public VenueServiceImpl(final VenueRepository venueRepository,
                            final StartTimeRepository startTimeRepository,
                            final AreaRepository areaRepository,
                            final CoordinatesRepository coordinatesRepository,
                            final ArtistRepository artistRepository,
                            final EventRepository eventRepository) {
        super(venueRepository, startTimeRepository, areaRepository, coordinatesRepository, artistRepository, eventRepository);
    }

    @Override
    public Venue create(final Venue venue) {
        return createVenue(venue);
    }

    @Override
    public Venue merge(final Venue venue) {
        return mergeVenue(venue);
    }

    @Override
    public void delete(final Venue venue) {
        venueRepository.delete(venue);
    }

    @Override
    public Venue findById(final Long id, final int depth) {
        return venueRepository.findOne(id, depth);
    }

    @Override
    public List<Venue> findAll() {
        final Iterable<Venue> venues = venueRepository.findAll();
        return Lists.newArrayList(venues);
    }

    @Override
    public void deleteAll() {
        venueRepository.deleteAll();
    }

    @Override
    public Venue findByName(final String name) {
        return findVenueByVenueName(name);
    }

    @Override
    public Venue findByNameAndCity(final String name, final String city) {
        return findVenueByNameAndCity(name, city);
    }

    @Override
    public List<Venue> findByFormattedCoordinates(final Double formattedLatitude, final Double formattedLongitude) {
        return findVenuesByFormattedCoordinates(formattedLatitude, formattedLongitude);
    }

    @Override
    public List<Venue> findByCoordinatesBetween(final Double minLat, final Double maxLat, final Double minLng, final Double maxLng) {
        return findVenuesByCoordinatesBetween(minLat, maxLat, minLng, maxLng);
    }

    @Override
    public Venue findByLatLng(final String latLng) {
        return findVenueByLatLng(latLng);
    }

    @Override
    public void createConstraint() {
        venueRepository.createConstraint();
    }
}
