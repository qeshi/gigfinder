package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Coordinates;

public interface CoordinatesService extends GenericService<Coordinates> {

    Coordinates findByFormattedCoordinates(final Double formattedLatitude, final Double formattedLongitude);

    Coordinates findByLatLng(final String latLng);

    void createConstraint();
}
