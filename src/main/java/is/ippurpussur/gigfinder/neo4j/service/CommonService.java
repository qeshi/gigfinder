package is.ippurpussur.gigfinder.neo4j.service;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.*;
import is.ippurpussur.gigfinder.model.parser.DomainParser;
import is.ippurpussur.gigfinder.model.parser.EntityUpdater;
import is.ippurpussur.gigfinder.model.parser.Key;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service()
public abstract class CommonService {

    final VenueRepository venueRepository;
    final StartTimeRepository startTimeRepository;
    final AreaRepository areaRepository;
    final CoordinatesRepository coordinatesRepository;
    final ArtistRepository artistRepository;
    final EventRepository eventRepository;
    final EntityUpdater entityUpdater;
    final DomainParser domainParser;

    @Autowired
    CommonService(final VenueRepository venueRepository,
                  final StartTimeRepository startTimeRepository,
                  final AreaRepository areaRepository,
                  final CoordinatesRepository coordinatesRepository,
                  final ArtistRepository artistRepository,
                  final EventRepository eventRepository) {
        this.venueRepository = venueRepository;
        this.startTimeRepository = startTimeRepository;
        this.areaRepository = areaRepository;
        this.coordinatesRepository = coordinatesRepository;
        this.artistRepository = artistRepository;
        this.eventRepository = eventRepository;
        entityUpdater = new EntityUpdater();
        domainParser = new DomainParser();
    }

    @Transactional
    Venue createVenue(final Venue venue) {
        final Venue venueToSave;
        final Area area = venue.getArea();
        final Venue venueToUpdate = findVenueToUpdate(venue);
        final Area areaOfVenueToUpdate = venueToUpdate != null ? venueToUpdate.getArea() : null;
        final Area areaToSet = area != null ? createArea(area) : areaOfVenueToUpdate;

        if (venueToUpdate != null) {
            venueToSave = entityUpdater.updateVenue(venueToUpdate, venue, areaToSet);
        } else {
            venueToSave = venue;
            venueToSave.setArea(areaToSet);
        }

        return mergeVenue(venueToSave);
    }

    @Transactional
    Venue mergeVenue(final Venue venue) {
        String uuid = venue.getUuid();
        final String name = venue.getName();
        final String website = venue.getWebsite();
        final String phone = venue.getPhone();
        final String description = venue.getDescription();
        final String latLng = venue.getLatLng();
        final Venue savedVenue;

        if(uuid != null && latLng == null){
            savedVenue = venueRepository.findByUuid(uuid);
        } else if (uuid != null) {
            savedVenue = venueRepository.merge(uuid, name, website, phone, description, latLng);
        } else {
            savedVenue = venueRepository.create(name, website, phone, description, latLng);
        }

        final Area area = venue.getArea();
        if (area != null) {
            venueRepository.updateRelationshipWithArea(latLng);
        }

        return venueRepository.findOne(savedVenue.getId(), 3);
    }

    @Transactional
    Venue updateVenue(final Venue venueToUpdate, final Venue newVenue) {
        final Venue alreadyExistingVenue = findVenueToUpdate(newVenue);
        final Venue venueToCompare = alreadyExistingVenue != null ? alreadyExistingVenue : newVenue;
        final Area newArea = newVenue.getArea();
        final Area areaToUpdate = venueToUpdate.getArea();
        final Area areaToSet;
        if (areaToUpdate == null) {
            areaToSet = newArea != null ? createArea(newArea) : null;
        } else {
            areaToSet = updateArea(areaToUpdate, newArea);
        }
        final Venue venue = entityUpdater.updateVenue(venueToUpdate, venueToCompare, areaToSet);

        return mergeVenue(venue);
    }

    StartTime createStartTime(final StartTime startTime) {
        StartTime startTimeToSave = new StartTime(startTime);
        final String dateTime = startTime.getDateTime();

        StartTime startTimeByDateTime = findStartTimeByDateTime(dateTime);
        if (startTimeByDateTime != null) {
            startTimeToSave = startTimeByDateTime;
        } else {
            final List<StartTime> existingStartTimes = findStartTimesByDate(startTime.getDate());
            final List<Event> eventsByStartDateAndTime =
                    findEventsByStartDateAndTime(startTime.getDate(), startTime.getTime());

            final StartTime existingStartTime = entityUpdater.findStartTimeToUpdate(existingStartTimes, startTime);
            final boolean startTimeIsSharedWithAnotherEvent = eventsByStartDateAndTime.size() >= 2;

            if (existingStartTime != null && !startTimeIsSharedWithAnotherEvent) {
                startTimeToSave = entityUpdater.updateStartTime(existingStartTime, startTime);
            }
        }

        return mergeStartTime(startTimeToSave);
    }

    StartTime mergeStartTime(final StartTime startTime) {
        final String uuid = startTime.getUuid();
        final String dateTime = startTime.getDateTime();
        final String date = startTime.getDate();
        final String time = startTime.getTime();

        if (uuid != null) {
            return startTimeRepository.merge(uuid, dateTime, date, time);
        }
        return startTimeRepository.create(dateTime, date, time);
    }

    @Transactional
    Area createArea(final Area area) {
        final Area areaToSave;
        final Area areaToUpdate = findAreaToUpdate(area);
        final Coordinates coordinates = area.getCoordinates();
        final Coordinates coordinatesOfAreaToUpdate = areaToUpdate != null ? areaToUpdate.getCoordinates() : null;
        final Coordinates coordinatesToSet =
                coordinates != null ? createCoordinates(coordinates) : coordinatesOfAreaToUpdate;

        if (areaToUpdate != null) {
            areaToSave = entityUpdater.updateArea(areaToUpdate, area, coordinatesToSet);
        } else {
            areaToSave = area;
            areaToSave.setCoordinates(coordinatesToSet);
        }

        return mergeArea(areaToSave);
    }

    @Transactional
    Area mergeArea(final Area area) {
        String uuid = area.getUuid();
        final String street = area.getStreet();
        final String zip = area.getZip();
        final String city = area.getCity();
        final String adminAreaLevel1 = area.getAdminAreaLevel1();
        final String country = area.getCountry();
        final String latLng = area.getLatLng();
        final String address = area.getAddress();
        final Coordinates coordinates = area.getCoordinates();
        final Area createdArea;

        if (uuid != null) {
            createdArea = areaRepository.merge(uuid, street, zip, city, adminAreaLevel1, country, latLng, address);
        } else {
            createdArea = areaRepository.create(street, zip, city, adminAreaLevel1, country, latLng, address);
        }

        if (coordinates != null) {
            areaRepository.updateRelationshipWithCoordinates(latLng);
        }

        if (createdArea != null) {
            return areaRepository.findOne(createdArea.getId());
        }
        return null;
    }

    @Transactional
    private Area updateArea(final Area areaToUpdate, final Area newArea) {
        final Coordinates newCoordinates = newArea.getCoordinates();
        final Coordinates coordinatesToUpdate = areaToUpdate.getCoordinates();

        Coordinates coordinatesToSet;
        if (coordinatesToUpdate == null) {
            coordinatesToSet = newCoordinates != null ? newCoordinates : null;
        } else {
            coordinatesToSet = updateCoordinates(coordinatesToUpdate, newCoordinates);
        }

        final Area area = entityUpdater.updateArea(areaToUpdate, newArea, coordinatesToSet);

        return mergeArea(area);
    }

    @Transactional
    Coordinates createCoordinates(final Coordinates coordinates) {
        Coordinates coordinatesToSave = coordinates;

        final Coordinates coordinatesByLatLng = findCoordinatesByLatLng(coordinates.getLatLng());
        if (coordinatesByLatLng != null) {
            coordinatesToSave = coordinatesByLatLng;
        }

        return mergeCoordinates(coordinatesToSave);
    }

    Coordinates mergeCoordinates(final Coordinates coordinates) {
        final String uuid = coordinates.getUuid();
        final Double latitude = coordinates.getLatitude();
        final Double longitude = coordinates.getLongitude();
        final Double formattedLatitude = coordinates.getFormattedLatitude();
        final Double formattedLongitude = coordinates.getFormattedLongitude();
        final String latLng = coordinates.getLatLng();

        if (uuid != null) {
            return coordinatesRepository.merge(uuid, latitude, longitude, formattedLatitude, formattedLongitude, latLng);
        }
        return coordinatesRepository.create(latLng, latitude, longitude, formattedLatitude, formattedLongitude);
    }

    private Coordinates updateCoordinates(final Coordinates coordinatesToUpdate, final Coordinates newCoordinates) {
        final Coordinates coordinates = entityUpdater.updateCoordinates(coordinatesToUpdate, newCoordinates);
        return mergeCoordinates(coordinates);
    }

    Artist createArtist(final Artist artistToCreate) {
        Artist artistToSave = new Artist(artistToCreate);
        Artist artistToUpdate;

        final String mbid = artistToCreate.getMbid();
        final String website = artistToCreate.getWebsite();

        if (mbid != null) {
            artistToUpdate = findArtistByMbid(artistToCreate.getMbid());
        } else {
            final List<Artist> existingArtists = findArtistsByArtistName(artistToCreate.getName());
            final Map<Artist, Integer> numberOfEvents = checkNumberOfEachArtistsUpcomingEvents(existingArtists);
            artistToUpdate = entityUpdater.findArtistToUpdate(existingArtists, artistToCreate, numberOfEvents);
        }
        if (artistToUpdate != null) {
            if (domainParser.stringHasValue(website)) {
                artistToUpdate.setWebsite(website);
            }
            artistToSave = new Artist(artistToUpdate);
        }

        return mergeArtist(artistToSave);
    }

    Artist mergeArtist(final Artist artist) {
        final String uuid = artist.getUuid();
        final String name = artist.getName();
        final String mbid = artist.getMbid();
        final String website = artist.getWebsite();

        if (uuid != null) {
            return artistRepository.merge(uuid, name, mbid, website);
        }
        return artistRepository.create(name, mbid, website);
    }

    @Transactional
    List<Artist> createArtists(final List<Artist> artists) {
        final List<Artist> createdArtists = new ArrayList<>();
        artists.forEach(artist -> {
            final Artist createdArtist = createArtist(artist);
            createdArtists.add(createdArtist);
        });

        return createdArtists;
    }

    List<Venue> findVenuesByFormattedCoordinates(final Double formattedLatitude, final Double formattedLongitude) {
        final Collection<Venue> venues = venueRepository.findByformattedCoordinates(formattedLatitude, formattedLongitude);
        return Lists.newArrayList(venues);
    }

    Venue findVenueByVenueName(final String venueName) {
        final Venue venue = venueRepository.findByName(venueName);
        if (venue != null) {
            return venueRepository.findOne(venue.getId(), 2);
        }
        return null;
    }

    StartTime findStartTimeByDateTime(final String dateTime) {
        return startTimeRepository.findByDateTime(dateTime);
    }

    List<StartTime> findStartTimesByDate(final String date) {
        final Collection<StartTime> startTimes = startTimeRepository.findByDate(date);
        return Lists.newArrayList(startTimes);
    }

    List<Event> findEventsByStartDateAndTime(final String date, final String time) {
        final Collection<Event> events = eventRepository.findByStartTimeDateAndStartTimeTime(date, time);
        return Lists.newArrayList(events);
    }

    List<Area> findAreasByFormattedCoordinates(final Double formattedLatitude, final Double formattedLongitude) {
        final Collection<Area> areas = areaRepository.findByCoordinatesFormattedLatitudeAndCoordinatesFormattedLongitude(
                formattedLatitude, formattedLongitude);
        return Lists.newArrayList(areas);
    }

    Coordinates findCoordinatesByFormattedCoordinates(final Double formattedLatitude, final Double formattedLongitude) {
        return coordinatesRepository.findByFormattedLatitudeAndFormattedLongitude(formattedLatitude, formattedLongitude);
    }

    Artist findArtistByMbid(final String mbid) {
        return artistRepository.findByMbid(mbid);
    }

    List<Artist> findArtistsByArtistName(final String artistName) {
        final Collection<Artist> artists = artistRepository.findByName(artistName);
        return Lists.newArrayList(artists);
    }

    List<Event> findEventsByArtistName(final String name) {
        final Collection<Event> events = eventRepository.findByArtistName(name);
        return Lists.newArrayList(events);
    }

    List<Venue> findVenuesByCoordinatesBetween(
            final Double minLat, final Double maxLat, final Double minLng, final Double maxLng) {
        final Collection<Venue> venues = venueRepository.findByCoordinatesBetween(minLat, maxLat, minLng, maxLng);
        return Lists.newArrayList(venues);
    }

    private List<Venue> findVenuesByCoordinates(final Coordinates coordinates) {
        final List<Venue> venues = new ArrayList<>();
        final Double latitude = coordinates.getLatitude();
        final Double longitude = coordinates.getLongitude();
        final Double errorRange = 2.0;
        final Map<String, Double> coordinatesWithinAllowableRange = domainParser.setAllowableRangeForCoordinates(latitude, longitude, errorRange);

        final List<Venue> closeVenues = findVenuesByCoordinatesBetween(
                coordinatesWithinAllowableRange.get(Key.MIN_LAT),
                coordinatesWithinAllowableRange.get(Key.MAX_LAT),
                coordinatesWithinAllowableRange.get(Key.MIN_LNG),
                coordinatesWithinAllowableRange.get(Key.MAX_LNG)
        );

        closeVenues.forEach(closeVenue -> {
            final Venue venueWithArea = venueRepository.findOne(closeVenue.getId(), 2);
            venues.add(venueWithArea);
        });

        return venues;
    }

    private Venue findVenueByUuid(final String uuid) {
        return venueRepository.findByUuid(uuid);
    }

    Venue findVenueByLatLng(final String latLng) {
        return venueRepository.findByLatLng(latLng);
    }

    Venue findVenueByNameAndCity(final String name, final String city) {
        return venueRepository.findByNameAndCity(name, city);
    }

    private Map<Artist, Integer> checkNumberOfEachArtistsUpcomingEvents(final List<Artist> artists) {
        final Map<Artist, Integer> numberOfEventsForArtists = new HashMap<>();
        artists.forEach(artist -> {
            final List<Event> events = findEventsByArtistName(artist.getName());
            numberOfEventsForArtists.put(artist, events.size());
        });
        return numberOfEventsForArtists;
    }

    Coordinates findCoordinatesByLatLng(final String latLng) {
        return coordinatesRepository.findByLatLng(latLng);
    }

    Area findAreaByLatLng(final String latLng) {
        return areaRepository.findByLatLng(latLng);
    }

    private Venue findVenueToUpdate(final Venue venue) {
        Venue venueToUpdate = null;
        final String uuid = venue.getUuid();
        final String latLng = venue.getLatLng();
        final String venueName = venue.getName();
        final Area area = venue.getArea();
        final Coordinates coordinates = area != null ? area.getCoordinates() : null;
        final String city = area != null ? area.getCity() : null;
        final Set<Venue> existingVenues = new HashSet<>();

        if (uuid != null) {
            venueToUpdate = findVenueByUuid(uuid);
        }
        if (venueToUpdate == null && latLng != null) {
            venueToUpdate = findVenueByLatLng(latLng);
        }
        if (venueToUpdate == null && venueName != null && city != null) {
            venueToUpdate = findVenueByNameAndCity(venueName, city);
        }
        if (venueToUpdate == null) {
            if (coordinates != null) {
                final List<Venue> venuesByCoordinates = findVenuesByCoordinates(coordinates);
                existingVenues.addAll(venuesByCoordinates);
            }
            if (existingVenues.isEmpty()) {
                final Venue venueByName = findVenueByVenueName(venue.getName());
                if (venueByName != null) {
                    existingVenues.add(venueByName);
                }
            }
            for (final Venue existingVenue : existingVenues) {
                if (domainParser.venuesAreSame(existingVenue, venue)) {
                    venueToUpdate = existingVenue;
                }
            }
        }
        return venueToUpdate;
    }

    private Area findAreaToUpdate(final Area area) {
        final Coordinates coordinates = area.getCoordinates();
        Area areaToUpdate = null;
        final Set<Area> existingAreas = new HashSet<>();

        final Area areaByLatLng = findAreaByLatLng(area.getLatLng());
        if (areaByLatLng != null) {
            areaToUpdate = areaByLatLng;
        } else if (coordinates != null) {
            final List<Area> areasByFormattedCoordinates =
                    findAreasByFormattedCoordinates(coordinates.getFormattedLatitude(), coordinates.getFormattedLongitude());
            existingAreas.addAll(areasByFormattedCoordinates);
            areaToUpdate = entityUpdater.findAreaToUpdate(new ArrayList<>(existingAreas), area);
        }

        return areaToUpdate;
    }
}
