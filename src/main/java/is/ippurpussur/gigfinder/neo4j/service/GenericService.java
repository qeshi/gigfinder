package is.ippurpussur.gigfinder.neo4j.service;

import java.util.List;

public interface GenericService<T> {

    T create(final T object);

    T merge(final T object);

    void delete(final T object);

    T findById(final Long id, int depth);

    List<T> findAll();

    void deleteAll();
}
