package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Venue;

import java.util.List;

public interface VenueService extends GenericService<Venue> {

    Venue merge(final Venue venue);

    Venue findByName(final String name);

    Venue findByNameAndCity(final String name, final String city);

    List<Venue> findByFormattedCoordinates(final Double formattedLatitude, final Double formattedLongitude);

    List<Venue> findByCoordinatesBetween(final Double minLat, final Double maxLat, final Double minLng, final Double maxLng);

    Venue findByLatLng(final String latLng);

    void createConstraint();
}
