package is.ippurpussur.gigfinder.neo4j.service;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.*;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service("EventService")
public final class EventServiceImpl extends CommonService implements EventService {

    @Autowired
    public EventServiceImpl(final VenueRepository venueRepository,
                            final StartTimeRepository startTimeRepository,
                            final AreaRepository areaRepository,
                            final CoordinatesRepository coordinatesRepository,
                            final ArtistRepository artistRepository,
                            final EventRepository eventRepository) {
        super(venueRepository, startTimeRepository, areaRepository, coordinatesRepository, artistRepository, eventRepository);
    }

    @Transactional
    @Override
    public Event create(final Event eventToCreate) {
        final Event eventToUpdate = findEventToUpdate(eventToCreate);
        final Event eventToSave = createEventToSave(eventToUpdate, eventToCreate);

        return merge(eventToSave);
    }

    @Override
    public Event merge(final Event event) {
        final String uuid = event.getUuid();
        final Long songkickId = event.getSongkickId();
        final Long bandsintownId = event.getBandsintownId();
        final String name = event.getName();
        final Event createdEvent;

        if (uuid != null) {
            createdEvent = eventRepository.merge(uuid, songkickId, bandsintownId, name);
        } else {
            createdEvent = eventRepository.create(name, songkickId, bandsintownId);
        }

        updateRelationships(createdEvent, event);

        return eventRepository.findOne(createdEvent.getId());
    }

    private void updateRelationships(final Event createdEvent, final Event eventToCreate) {
        final String uuid = createdEvent.getUuid();
        final Venue venue = eventToCreate.getVenue();

        if (venue != null) {
            eventRepository.updateRelationshipWithVenue(uuid, venue.getUuid());
        }

        final List<Artist> artists = eventToCreate.getArtists();
        if (artists != null && !artists.isEmpty()) {
            artists.forEach(artist -> eventRepository.updateRelationshipWithArtist(uuid, artist.getUuid()));
        }

        final StartTime startTime = eventToCreate.getStartTime();
        if (startTime != null) {
            eventRepository.updateRelationshipWithStartTime(uuid, startTime.getUuid());
        }
    }

    @Override
    public void delete(final Event event) {
        eventRepository.delete(event);
    }

    @Override
    public Event findById(final Long id, final int depth) {
        return eventRepository.findOne(id, depth);
    }

    @Override
    public List<Event> findAll() {
        final Iterable<Event> events = eventRepository.findAll();
        return Lists.newArrayList(events);
    }

    @Override
    public List<Event> findAllSortByStartDateTime(int pageNumber, int numberOfResultsPerPage) {
        final int pagesToSkip = pageNumber - 1;
        final int resultsToSkip = pagesToSkip * numberOfResultsPerPage;
        final Collection<Event> events = eventRepository.findAllSortByStartDateTime(resultsToSkip, numberOfResultsPerPage);
        return Lists.newArrayList(events);
    }

    @Transactional
    @Override
    public List<Event> createMany(final List<Event> events) {
        final List<Event> createdEvents = new ArrayList<>();
        events.forEach(event -> {
            final Event createdEvent = create(event);
            createdEvents.add(createdEvent);
        });
        return createdEvents;
    }

    @Override
    public void deleteAll() {
        eventRepository.deleteAll();
    }

    @Override
    public List<Event> findByName(final String name) {
        final Collection<Event> events = eventRepository.findByName(name);
        return Lists.newArrayList(events);
    }

    @Override
    public List<Event> findByNameLike(final String name) {
        final Collection<Event> events = eventRepository.findByNameLike(name);
        return Lists.newArrayList(events);
    }

    @Override
    public List<Event> findByStartDateAndStartTime(final String date, final String time) {
        return findEventsByStartDateAndTime(date, time);
    }

    @Override
    public List<Event> findByArtistName(final String name) {
        return findEventsByArtistName(name);
    }

    @Override
    public List<Event> findByStartDate(String date) {
        final Collection<Event> events = eventRepository.findByStartTimeDate(date);
        return Lists.newArrayList(events);
    }

    @Override
    public List<Event> findByCity(final String city, final int pageNumber, final int numberOfResultsPerPage) {
        final int pagesToSkip = pageNumber - 1;
        final int resultsToSkip = pagesToSkip * numberOfResultsPerPage;
        final Collection<Event> events = eventRepository.findByCity(city, resultsToSkip, numberOfResultsPerPage);
        return Lists.newArrayList(events);
    }

    @Override
    public List<Event> findByCityAndNameLike(final String city, final String name) {
        final Collection<Event> events = eventRepository.findByCityAndNameLike(city, name);
        return Lists.newArrayList(events);
    }

    @Override
    public int countEventsByCity(final String city) {
        return eventRepository.countEventsByCity(city);
    }

    @Override
    public int countAllEvents() {
        return eventRepository.countAllEvents();
    }


    @Override
    public Event findBySongkickId(final Long songkickId) {
        return eventRepository.findBySongkickId(songkickId);
    }

    @Override
    public Event findByBandsintownId(final Long bandsintownId) {
        return eventRepository.findByBandsintownId(bandsintownId);
    }

    @Override
    public void createSongkickIdAsUniqueConstraint() {
        eventRepository.createSongkickIdAsUniqueConstraint();
    }

    @Override
    public void createBandsintownIdAsUniqueConstraint() {
        eventRepository.createBandsintownIdAsUniqueConstraint();
    }

    private Set<Event> findManyById(final Set<Event> events, int depth) {
        final Set<Event> eventSet = new HashSet<>();
        events.forEach(event -> {
            final Event eventById = findById(event.getId(), depth);
            eventSet.add(eventById);
        });
        return eventSet;
    }

    private Event findEventToUpdate(final Event event) {
        int depth = 3;

        final Event eventBySongkickId = findEventToUpdateBySongkickId(event.getSongkickId(), depth);
        if (eventBySongkickId != null) {
            return eventBySongkickId;
        }

        final Event eventByBandsintownId = findEventToUpdateByBandsintownId(event.getBandsintownId(), depth);
        if (eventByBandsintownId != null) {
            return eventByBandsintownId;
        }

        return findEventToUpdateByDateOrNameAndCityOrName(event, depth);
    }

    private Event findEventToUpdateBySongkickId(final Long songkickId, final int depth) {
        final Event eventBySongkickId = songkickId != null ? findBySongkickId(songkickId) : null;
        if (eventBySongkickId != null) {
            return findById(eventBySongkickId.getId(), depth);
        }
        return null;
    }

    private Event findEventToUpdateByBandsintownId(final Long bandsintownId, final int depth) {
        final Event eventByBandsintownId = bandsintownId != null ? findByBandsintownId(bandsintownId) : null;
        if (eventByBandsintownId != null) {
            return findById(eventByBandsintownId.getId(), depth);
        }
        return null;
    }

    private Event findEventToUpdateByDateOrNameAndCityOrName(final Event event, final int depth) {
        final Set<Event> events = new HashSet<>();

        final StartTime startTime = event.getStartTime();
        final List<Event> eventsByDate = findByStartDate(startTime.getDate());
        events.addAll(eventsByDate);

        final String eventName = event.getName();
        final String simpleEventName = eventName != null ? domainParser.simplifyEventName(eventName) : null;
        final Venue venue = event.getVenue();
        final Area area = venue != null ? venue.getArea() : null;
        final String city = area != null ? area.getCity() : null;
        if (city != null) {
            final List<Event> eventsByCityAndName = findByCityAndNameLike(city, simpleEventName);
            events.addAll(eventsByCityAndName);
        }
        if (events.isEmpty()) {
            final List<Event> eventsByName = findByNameLike(simpleEventName);
            events.addAll(eventsByName);
        }

        final Set<Event> eventsById = findManyById(events, depth);

        return entityUpdater.findEventToUpdate(Lists.newArrayList(eventsById), event);
    }

    private Event createEventToSave(final Event eventToUpdate, final Event eventToCreate) {
        final Event eventToSave;
        final List<Artist> artists = eventToCreate.getArtists();
        final StartTime startTime = eventToCreate.getStartTime();
        final Venue newVenue = eventToCreate.getVenue();

        final List<Artist> artistsToSet = artists != null ? createArtists(artists) : null;
        final StartTime startTimeToSet = createStartTime(startTime);

        if (eventToUpdate != null) {
            final Venue venueToUpdate = eventToUpdate.getVenue();
            final Venue venueToSet = venueToUpdate != null ? updateVenue(venueToUpdate, newVenue) : createVenue(newVenue);
            eventToSave = entityUpdater.updateEvent(eventToUpdate, eventToCreate, artistsToSet, venueToSet, startTimeToSet);
        } else {
            final Venue createdVenue = createVenue(newVenue);
            eventToSave = new Event(eventToCreate);
            eventToSave.setVenue(createdVenue);
            eventToSave.setArtists(artistsToSet);
            eventToSave.setStartTime(startTimeToSet);
        }

        return eventToSave;
    }
}
