package is.ippurpussur.gigfinder.neo4j.service;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.User;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("UserService")
public final class UserServiceImpl extends CommonService implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository,
                           final VenueRepository venueRepository,
                           final StartTimeRepository startTimeRepository,
                           final AreaRepository areaRepository,
                           final CoordinatesRepository coordinatesRepository,
                           final ArtistRepository artistRepository,
                           final EventRepository eventRepository) {
        super(venueRepository, startTimeRepository, areaRepository, coordinatesRepository, artistRepository, eventRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User create(final User user) {
        User userToSave = user;
        final Set<Artist> artists = user.getTrackingArtists();
        final List<Artist> updatedArtists =
                artists != null ? createArtists(new ArrayList<>(artists)) : new ArrayList<>();

        final User existingUser = findByName(user.getName());
        if (existingUser != null) {
            userToSave = existingUser;
        }
        userToSave.addTrackingArtists(new HashSet<>(updatedArtists));

        return merge(userToSave);
    }

    @Override
    public User merge(final User user) {
        final String uuid = user.getUuid();
        final String name = user.getName();
        final Set<Artist> trackingArtists = user.getTrackingArtists();
        final User createdUser;

        if (uuid != null) {
            createdUser = userRepository.merge(uuid, name);
        } else {
            createdUser = userRepository.create(name);
        }

        if (trackingArtists != null && !trackingArtists.isEmpty()) {
            trackingArtists.forEach(artist -> userRepository.updateRelationshipWithArtist(
                    createdUser.getUuid(), artist.getUuid()));
        }

        return userRepository.findOne(createdUser.getId());
    }

    @Override
    public void delete(final User user) {
        userRepository.delete(user);
    }

    @Override
    public User findById(final Long id, final int depth) {
        return userRepository.findOne(id, depth);
    }

    @Override
    public List<User> findAll() {
        final Iterable<User> users = userRepository.findAll();
        return Lists.newArrayList(users);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public User findByName(final String name) {
        return userRepository.findByName(name);
    }

    @Override
    public void createConstraint() {
        userRepository.createConstraint();
    }
}
