package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Artist;

import java.util.List;

public interface ArtistService extends GenericService<Artist> {

    Artist findByMbid(final String mbid);

    List<Artist> findByName(final String name);

    List<Artist> createMany(final List<Artist> artists);

    void createConstraint();
}
