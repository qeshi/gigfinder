package is.ippurpussur.gigfinder.neo4j.repository;

import is.ippurpussur.gigfinder.model.neo4j.Area;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface AreaRepository extends GraphRepository<Area> {

    @Query("CREATE (" +
            "a:Area {latLng: {latLng}, " +
            "uuid: apoc.create.uuid(), " +
            "street: {street}, " +
            "zip: {zip}, " +
            "city: {city}, " +
            "adminAreaLevel1: {adminAreaLevel1}," +
            "country: {country}," +
            "latLng: {latLng}," +
            "address: {address}}) " +
            "RETURN a")
    Area create(@Param("street") final String street,
                @Param("zip") final String zip,
                @Param("city") final String city,
                @Param("adminAreaLevel1") final String adminAreaLevel1,
                @Param("country") final String country,
                @Param("latLng") final String latLng,
                @Param("address") final String address);

    @Query("MERGE (a:Area {latLng: {latLng}}) SET " +
            "a.uuid = {uuid}, " +
            "a.street = {street}, " +
            "a.zip = {zip}, " +
            "a.city = {city}, " +
            "a.adminAreaLevel1 = {adminAreaLevel1}, " +
            "a.country = {country}, " +
            "a.address = {address} " +
            "RETURN a")
    Area merge(@Param("uuid") final String uuid,
               @Param("street") final String street,
               @Param("zip") final String zip,
               @Param("city") final String city,
               @Param("adminAreaLevel1") final String adminAreaLevel1,
               @Param("country") final String country,
               @Param("latLng") final String latLng,
               @Param("address") final String address);

    @Query("MATCH (a:Area) WHERE a.latLng = {latLng} WITH a MATCH (c:Coordinates) WHERE c.latLng = {latLng} " +
            "MERGE (a)-[:COORDINATES]->(c) RETURN a")
    Area updateRelationshipWithCoordinates(@Param("latLng") final String latLng);

    Collection<Area> findByCoordinatesFormattedLatitudeAndCoordinatesFormattedLongitude(
            final Double formattedLatitude, final Double formattedLongitude);

    @Query("MATCH (a:Area)--(c:Coordinates) WHERE c.formattedLatitude >= {minLat} AND c.formattedLatitude <= {maxLat} " +
            "AND c.formattedLongitude >= {minLng} AND c.formattedLongitude <= {maxLng} RETURN a")
    Collection<Area> findByCoordinatesBetween(@Param("minLat") final Double minLat,
                                              @Param("maxLat") final Double maxLat,
                                              @Param("minLng") final Double minLng,
                                              @Param("maxLng") final Double maxLng);

    Area findByLatLng(final String latLng);

    @Query("CREATE CONSTRAINT ON (a:Area) ASSERT a.latLng IS UNIQUE")
    void createConstraint();
}
