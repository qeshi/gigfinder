package is.ippurpussur.gigfinder.neo4j.repository;

import is.ippurpussur.gigfinder.model.neo4j.StartTime;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface StartTimeRepository extends GraphRepository<StartTime> {

    @Query("CREATE (s:StartTime{uuid: apoc.create.uuid(), dateTime: {dateTime}, date: {date}, time:{time}}) RETURN s")
    StartTime create(
            @Param("dateTime") final String dateTime, @Param("date") final String date, @Param("time") final String time);

    @Query("MERGE (s:StartTime{uuid: {uuid}}) SET s.dateTime = {dateTime}, s.date = {date}, s.time = {time} RETURN s")
    StartTime merge(@Param("uuid") final String uuid,
                    @Param("dateTime") final String dateTime,
                    @Param("date") final String date,
                    @Param("time") final String time);

    Collection<StartTime> findByDate(final String date);

    StartTime findByDateTime(final String dateTime);

    @Query("CREATE CONSTRAINT ON (s:StartTime) ASSERT s.dateTime IS UNIQUE")
    void createConstraint();
}
