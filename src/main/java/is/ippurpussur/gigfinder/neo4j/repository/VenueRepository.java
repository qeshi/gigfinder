package is.ippurpussur.gigfinder.neo4j.repository;

import is.ippurpussur.gigfinder.model.neo4j.Venue;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface VenueRepository extends GraphRepository<Venue> {

    @Query("CREATE (v:Venue {" +
            "uuid : apoc.create.uuid(), " +
            "name: {name}, " +
            "website: {website}, " +
            "phone: {phone}, " +
            "description: {description}, " +
            "latLng: {latLng}}) " +
            "RETURN v")
    Venue create(@Param("name") final String name,
                 @Param("website") final String website,
                 @Param("phone") final String phone,
                 @Param("description") final String description,
                 @Param("latLng") final String latLng);

    @Query("MERGE (v:Venue {latLng: {latLng}}) SET " +
            "v.uuid = {uuid}, " +
            "v.name = {name}, " +
            "v.website = {website}, " +
            "v.phone = {phone}, " +
            "v.description = {description} " +
            "RETURN v")
    Venue merge(@Param("uuid") final String uuid,
                @Param("name") final String name,
                @Param("website") final String website,
                @Param("phone") final String phone,
                @Param("description") final String description,
                @Param("latLng") final String latLng);

    @Query("MATCH (v:Venue) WHERE v.latLng = {latLng} WITH v MATCH (a:Area) WHERE a.latLng = {latLng} " +
            "MERGE (v)-[:LOCATED_IN]->(a) RETURN v, a")
    Venue updateRelationshipWithArea(@Param("latLng") final String lagLng);

    Venue findByUuid(final String uuid);

    Venue findByName(final String name);

    Venue findByLatLng(final String latLng);

    @Query("MATCH (v:Venue)--(a:Area) WHERE v.name = {name} AND a.city = {city} RETURN v")
    Venue findByNameAndCity(@Param("name") final String name, @Param("city") final String city);

    @Query("MATCH (v:Venue)--(a:Area)--(c:Coordinates) WHERE c.formattedLatitude = {lat} AND c.formattedLongitude = {lng} RETURN v")
    Collection<Venue> findByformattedCoordinates(
            @Param("lat") final Double formattedLatitude, @Param("lng") final Double formattedLongitude);

    @Query("MATCH (v:Venue)--(a:Area)--(c:Coordinates) WHERE c.formattedLatitude >= {minLat} AND c.formattedLatitude <= {maxLat} " +
            "AND c.formattedLongitude >= {minLng} AND c.formattedLongitude <= {maxLng} RETURN v")
    Collection<Venue> findByCoordinatesBetween(@Param("minLat") final Double minLat,
                                               @Param("maxLat") final Double maxLat,
                                               @Param("minLng") final Double minLng,
                                               @Param("maxLng") final Double maxLng);

    @Query("CREATE CONSTRAINT ON (v:Venue) ASSERT v.latLng IS UNIQUE")
    void createConstraint();
}
