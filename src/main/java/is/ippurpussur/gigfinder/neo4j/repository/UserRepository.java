package is.ippurpussur.gigfinder.neo4j.repository;

import is.ippurpussur.gigfinder.model.neo4j.User;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends GraphRepository<User> {

    @Query("CREATE (u:User{uuid: apoc.create.uuid(), name: {name}}) RETURN u")
    User create(@Param("name") final String name);

    @Query("MERGE (u:User{uuid: {uuid}}) SET u.name = {name}, u.uuid = {uuid} RETURN u")
    User merge(@Param("uuid") final String uuid, @Param("name") final String name);

    @Query("MATCH (u:User{uuid: {userUuid}}), (a:Artist{uuid: {artistUuid}}) MERGE (u)-[:TRACKING]->(a) RETURN DISTINCT u")
    User updateRelationshipWithArtist(@Param("userUuid") final String userUuid, @Param("artistUuid") final String artistUuid);

    User findByName(final String name);

    @Query("CREATE CONSTRAINT ON (u:User) ASSERT u.name IS UNIQUE")
    void createConstraint();
}
