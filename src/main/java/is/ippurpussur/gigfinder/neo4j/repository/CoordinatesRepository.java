package is.ippurpussur.gigfinder.neo4j.repository;

import is.ippurpussur.gigfinder.model.neo4j.Coordinates;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

public interface CoordinatesRepository extends GraphRepository<Coordinates> {

    @Query("CREATE (c:Coordinates {" +
            "uuid: apoc.create.uuid(), " +
            "latitude: {latitude}, " +
            "longitude: {longitude}, " +
            "formattedLatitude: {formattedLatitude}, " +
            "formattedLongitude: {formattedLongitude}, " +
            "latLng: {latLng}}) " +
            "RETURN c")
    Coordinates create(
            @Param("latLng") final String latLng,
            @Param("latitude") final Double latitude,
            @Param("longitude") final Double longitude,
            @Param("formattedLatitude") final Double formattedLatitude,
            @Param("formattedLongitude") final Double formattedLongitude);

    @Query("MERGE (c:Coordinates {latLng: {latLng}}) SET " +
            "c.uuid = {uuid}, " +
            "c.latitude = {latitude}, " +
            "c.longitude = {longitude}, " +
            "c.formattedLatitude = {formattedLatitude}, " +
            "c.formattedLongitude = {formattedLongitude}" +
            "RETURN c")
    Coordinates merge(
            @Param("uuid") final String uuid,
            @Param("latitude") final Double latitude,
            @Param("longitude") final Double longitude,
            @Param("formattedLatitude") final Double formattedLatitude,
            @Param("formattedLongitude") final Double formattedLongitude,
            @Param("latLng") final String latLng);

    Coordinates findByFormattedLatitudeAndFormattedLongitude(final Double latitude, final Double longitude);

    Coordinates findByLatLng(final String latLng);

    @Query("CREATE CONSTRAINT ON (c:Coordinates) ASSERT c.latLng IS UNIQUE")
    void createConstraint();
}
