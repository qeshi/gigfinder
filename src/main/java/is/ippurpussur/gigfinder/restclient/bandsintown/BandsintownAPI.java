package is.ippurpussur.gigfinder.restclient.bandsintown;

import is.ippurpussur.gigfinder.model.bandsintown.BandsintownEvent;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface BandsintownAPI {

    @GET("mbid_{mbid}/events?format=json&api_version=2.0&app_id=YOUR_APP_ID")
    Call<List<BandsintownEvent>> getUpcomingEvents(@Path("mbid") final String mbid);
}
