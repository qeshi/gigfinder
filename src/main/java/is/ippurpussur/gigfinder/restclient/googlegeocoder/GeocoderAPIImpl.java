package is.ippurpussur.gigfinder.restclient.googlegeocoder;

import is.ippurpussur.gigfinder.model.googlegeocoder.AddressComponent;
import is.ippurpussur.gigfinder.model.googlegeocoder.GeocoderResponse;
import is.ippurpussur.gigfinder.model.googlegeocoder.Result;
import is.ippurpussur.gigfinder.model.parser.Key;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static is.ippurpussur.gigfinder.model.parser.Key.*;

public final class GeocoderAPIImpl {

    private final GeocoderAPI geocoderAPI;
    private final String apiKey;

    public GeocoderAPIImpl() {
        geocoderAPI = createService();
        final String envKeyName = "GEOCODER_API_KEY";
        apiKey = System.getenv(envKeyName);
    }

    private GeocoderAPI createService() {

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/api/geocode/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(GeocoderAPI.class);
    }

    private GeocoderResponse getAddressByCoordinates(final String langLng, final String languageCode) throws IOException {
        final Call<GeocoderResponse> call = geocoderAPI.getAddressByCoordinates(langLng, languageCode, apiKey);
        return call.execute().body();
    }

    public Map<String, String> getAddress(final Double latitude, final Double longitude, final String languageCode) throws IOException {
        final String latLng = formatLatLng(latitude, longitude);
        final GeocoderResponse geocoderResponse = getAddressByCoordinates(latLng, languageCode);

        if (geocoderResponse != null) {
            final List<AddressComponent> allAddressComponents = new ArrayList<>();
            final List<Result> results = geocoderResponse.getResults();
            results.forEach(result -> {
                final List<AddressComponent> addressComponents = result.getAddressComponents();
                allAddressComponents.addAll(addressComponents);
            });
            if (allAddressComponents.size() > 0) {
                return findLocalityAndAdminAreaLevel1AndCountry(allAddressComponents);
            }
        }
        return null;
    }

    private String formatLatLng(Double latitude, Double longitude) {
        return String.valueOf(latitude) + "," + String.valueOf(longitude);
    }

    private Map<String, String> findLocalityAndAdminAreaLevel1AndCountry(final List<AddressComponent> addressComponents) {
        final List<String> localities = new ArrayList<>();
        final List<String> countries = new ArrayList<>();
        final List<String> adminAreaLevel3List = new ArrayList<>();
        final List<String> adminAreaLevel1List = new ArrayList<>();

        createAddressLists(addressComponents, localities, countries, adminAreaLevel3List, adminAreaLevel1List);

        final Map<String, String> address = new HashMap<>();
        address.put(CITY, getCity(localities, adminAreaLevel3List));
        address.put(ADMIN_AREA_LEVEL_1, getAdminAreaLevel1(adminAreaLevel1List));
        address.put(COUNTRY, getCountry(countries));

        return address;
    }

    private void createAddressLists(final List<AddressComponent> addressComponents,
                                    final List<String> localities,
                                    final List<String> countries,
                                    final List<String> adminAreaLevel3List,
                                    final List<String> adminAreaLevel1List) {
        final String typeLocality = "locality";
        final String typeCountry = "country";
        final String typeAdminAreaLevel3 = "administrative_area_level_3";
        final String typeAdminAreaLevel1 = "administrative_area_level_1";

        addressComponents.forEach(addressComponent -> {
            final List<String> typesToCheck = addressComponent.getTypes();

            typesToCheck.forEach(typeToCheck -> {
                if (typeToCheck.equals(typeLocality)) {
                    localities.add(addressComponent.getName());
                }
                if (typeToCheck.equals(typeCountry)) {
                    countries.add(addressComponent.getName());
                }
                if (typeToCheck.equals(typeAdminAreaLevel3)) {
                    adminAreaLevel3List.add(addressComponent.getName());
                }
                if (typeToCheck.equals(typeAdminAreaLevel1)) {
                    adminAreaLevel1List.add(addressComponent.getName());
                }
            });
        });
    }

    private String getCity(final List<String> localities, final List<String> adminAreaLevel3List) {
        final int localitiesSize = localities.size();
        final int adminAreaLevel3ListSize = adminAreaLevel3List.size();

        if (localitiesSize > 0) {
            return localities.get(localitiesSize - 1);
        }
        if (adminAreaLevel3ListSize > 0) {
            return adminAreaLevel3List.get(adminAreaLevel3ListSize - 1);
        }
        return null;
    }

    private String getAdminAreaLevel1(final List<String> adminAreaLevel1List) {
        final int adminAreaLevel1ListSize = adminAreaLevel1List.size();

        if (adminAreaLevel1ListSize > 0) {
            return adminAreaLevel1List.get(adminAreaLevel1ListSize - 1);
        }
        return null;
    }

    private String getCountry(final List<String> countries) {
        final int countriesSize = countries.size();

        if (countriesSize > 0) {
            return countries.get(countriesSize - 1);
        }
        return null;
    }

    private GeocoderResponse getLatLngByAddress(final String address) throws IOException {
        final Call<GeocoderResponse> call = geocoderAPI.getLatLng(address, apiKey);
        return call.execute().body();
    }

    public Map<String, Double> getLatLng(final String address) throws IOException {
        final Map<String, Double> latLng = new HashMap<>();
        final GeocoderResponse geocoderResponse = getLatLngByAddress(address);
        if (geocoderResponse != null) {
            final List<Result> results = geocoderResponse.getResults();
            if (results.size() > 0) {
                final Double lat = results.get(0).getGeometry().getLocation().getLatitude();
                final Double lng = results.get(0).getGeometry().getLocation().getLongitude();
                latLng.put(Key.LAT, lat);
                latLng.put(Key.LNG, lng);
            }
        }

        return latLng;
    }
}
