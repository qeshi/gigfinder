package is.ippurpussur.gigfinder.restclient.googlegeocoder;

import is.ippurpussur.gigfinder.model.googlegeocoder.GeocoderResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GeocoderAPI {

    @GET("json")
    Call<GeocoderResponse> getAddressByCoordinates(@Query("latlng") final String latLng,
                                                   @Query("language") final String languageCode,
                                                   @Query("key") final String apiKey);

    @GET("json")
    Call<GeocoderResponse> getLatLng(@Query("address") final String address, @Query("key") final String apiKey);
}
