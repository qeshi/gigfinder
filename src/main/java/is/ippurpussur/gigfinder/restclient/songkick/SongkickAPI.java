package is.ippurpussur.gigfinder.restclient.songkick;

import is.ippurpussur.gigfinder.model.songkick.SongkickResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

interface SongkickAPI {

    @GET("artists/mbid:{artistMbid}/calendar.json")
    Call<SongkickResponse> getUpcomingEvents(@Path("artistMbid") final String artistMbid, @Query("apikey") final String apiKey);

    @GET("venues/{venueId}.json")
    Call<SongkickResponse> getVenueDetails(@Path("venueId") final Long venueId, @Query("apikey") final String apiKey);
}
