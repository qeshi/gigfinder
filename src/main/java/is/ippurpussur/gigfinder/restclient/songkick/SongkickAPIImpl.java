package is.ippurpussur.gigfinder.restclient.songkick;

import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.songkick.SongkickEvent;
import is.ippurpussur.gigfinder.model.songkick.SongkickResponse;
import is.ippurpussur.gigfinder.model.songkick.SongkickVenue;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class SongkickAPIImpl {

    private final SongkickAPI songkickAPI;
    private final String apiKey;

    public SongkickAPIImpl() {
        songkickAPI = createService();
        final String envKeyName = "SONGKICK_API_KEY";
        apiKey = System.getenv(envKeyName);
    }

    private SongkickAPI createService() {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.songkick.com/api/3.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(SongkickAPI.class);
    }

    private SongkickResponse getSongkickUpcomingEventsResponse(final String artistMbid) throws IOException {
        final Call<SongkickResponse> call = songkickAPI.getUpcomingEvents(artistMbid, apiKey);
        return call.execute().body();
    }

    private List<SongkickEvent> getUpcomingEventsByArtistMbid(final String artistMbid) throws IOException {
        if(artistMbid != null) {
            final SongkickResponse songkickUpcomingEventsResponse = getSongkickUpcomingEventsResponse(artistMbid);
            final List<SongkickEvent> events = songkickUpcomingEventsResponse.getResultsPage().getResults().getEvents();
            if (events != null) {
                return events;
            }
        }
        return null;
    }

    public Set<SongkickEvent> getUpcomingEventsByMultipleArtists(final List<Artist> artists) {
        final Set<SongkickEvent> eventsByMultipleArtists = new HashSet<>();
        artists.forEach(artist -> {
            String mbid = artist.getMbid();
            if (mbid != null) {
                try {
                    final List<SongkickEvent> upcomingEventsByArtist = getUpcomingEventsByArtistMbid(mbid);
                    if (upcomingEventsByArtist != null) {
                        eventsByMultipleArtists.addAll(upcomingEventsByArtist);
                    }
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        });

        return eventsByMultipleArtists;
    }

    private SongkickResponse getSongkickVenueDetailsResponse(final Long songkickVenueId) throws IOException {
        final Call<SongkickResponse> call = songkickAPI.getVenueDetails(songkickVenueId, apiKey);
        return call.execute().body();
    }

    private SongkickVenue getVenueDetails(final Long songkickVenueId) throws IOException {
        final SongkickResponse songkickVenueDetailsResponse = getSongkickVenueDetailsResponse(songkickVenueId);
        return songkickVenueDetailsResponse.getResultsPage().getResults().getVenue();
    }

    public List<SongkickEvent> setVenueDetailsToEvents(final List<SongkickEvent> events) {
        events.forEach(event -> {
            try {
                final Long songkickVenueId = event.getVenue().getSongkickId();
                if (songkickVenueId != null) {
                    final SongkickVenue venue = getVenueDetails(songkickVenueId);
                    event.setVenue(venue);
                }
            } catch (final IOException e) {
                e.printStackTrace();
            }
        });
        return events;
    }
}
