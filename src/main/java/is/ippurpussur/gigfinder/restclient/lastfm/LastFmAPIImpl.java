package is.ippurpussur.gigfinder.restclient.lastfm;

import is.ippurpussur.gigfinder.model.lastfm.LastFmSimilarArtistsResponse;
import is.ippurpussur.gigfinder.model.lastfm.LastFmTopArtistsResponse;
import is.ippurpussur.gigfinder.model.lastfm.SimilarArtist;
import is.ippurpussur.gigfinder.model.lastfm.SimilarArtists;
import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.parser.DomainParser;
import is.ippurpussur.gigfinder.model.neo4j.MatchLevel;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class LastFmAPIImpl {

    private final LastFmAPI lastFmAPI;
    private final String apiKey;

    public LastFmAPIImpl() {
        lastFmAPI = createService();
        final String envKeyName = "LAST_FM_API_KEY";
        apiKey = System.getenv(envKeyName);
    }

    private LastFmAPI createService() {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ws.audioscrobbler.com/2.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(LastFmAPI.class);
    }

    private LastFmTopArtistsResponse getTopArtistsResponse(final String lastFmUsername) throws IOException {
        final Call<LastFmTopArtistsResponse> call = lastFmAPI.getTopArtists(lastFmUsername, apiKey);
        return call.execute().body();
    }

    public List<Artist> getTopArtistsByLastFmUsername(final String lastFmUsername) throws IOException {
        final LastFmTopArtistsResponse topArtistsResponse = getTopArtistsResponse(lastFmUsername);
        return topArtistsResponse.getTopArtists().getArtists();
    }

    private LastFmSimilarArtistsResponse getSimilarArtistsResponse(final String mbid, final int limit) throws IOException {
        final Call<LastFmSimilarArtistsResponse> call = lastFmAPI.getSimilarArtistsByMbid(mbid, limit, apiKey);
        return call.execute().body();
    }

    private List<SimilarArtist> getSimilarArtistsByMbid(final String mbid, final int limit) throws IOException {
        final LastFmSimilarArtistsResponse similarArtistsResponse = getSimilarArtistsResponse(mbid, limit);
        final SimilarArtists similarArtists = similarArtistsResponse.getSimilarArtists();
        return similarArtists != null ? similarArtistsResponse.getSimilarArtists().getSimilarArtists() : new ArrayList<>();
    }

    private MatchLevel createMatchLevel(final Artist artist, final SimilarArtist similarArtist) {
        final DomainParser domainParser = new DomainParser();
        final Double level = similarArtist.getMatchLevel();
        final Artist convertedSimilarArtist = domainParser.convertSimilarArtistToArtist(similarArtist);

        return new MatchLevel(level, artist, convertedSimilarArtist);
    }

    private Set<MatchLevel> createMatchLevels(final Artist artist, final int limit) throws IOException {
        final String mbid = artist.getMbid();
        final Set<MatchLevel> matchLevels = new HashSet<>();
        if (mbid != null) {
            final List<SimilarArtist> similarArtists = getSimilarArtistsByMbid(mbid, limit);
            similarArtists.forEach(similarArtist -> {
                final MatchLevel matchLevel = createMatchLevel(artist, similarArtist);
                matchLevels.add(matchLevel);
            });
        }
        return matchLevels;
    }

    public Set<MatchLevel> createMatchLevelsOfMultipleArtists(final List<Artist> artists, final int limit) {
        Set<MatchLevel> allMatchLevels = new HashSet<>();
        artists.forEach(artist -> {
            try {
                final Set<MatchLevel> matchLevels = createMatchLevels(artist, limit);
                allMatchLevels.addAll(matchLevels);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return allMatchLevels;
    }
}
