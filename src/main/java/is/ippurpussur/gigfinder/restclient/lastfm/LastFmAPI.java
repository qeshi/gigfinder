package is.ippurpussur.gigfinder.restclient.lastfm;

import is.ippurpussur.gigfinder.model.lastfm.LastFmSimilarArtistsResponse;
import is.ippurpussur.gigfinder.model.lastfm.LastFmTopArtistsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface LastFmAPI {

    @GET("?method=user.gettopartists&format=json")
    Call<LastFmTopArtistsResponse> getTopArtists(@Query("user") final String user, @Query("api_key") final String apiKey);

    @GET("?method=artist.getsimilar&format=json")
    Call<LastFmSimilarArtistsResponse> getSimilarArtistsByMbid(
            @Query("mbid") final String mbid, @Query("limit") final int limit, @Query("api_key") final String apiKey);
}
