package is.ippurpussur.gigfinder.model.parser;

public final class Key {

    public static String LAT = "LAT";
    public static String LNG = "LNG";
    static String NAME = "NAME";
    public static String CITY = "CITY";
    public static String ADMIN_AREA_LEVEL_1 = "ADMIN_AREA_LEVEL_1";
    public static String COUNTRY = "COUNTRY";
    static String AREA = "AREA";
    static String WEBSITE = "WEBSITE";
    static String PHONE = "PHONE";
    static String DESCRIPTION = "DESCRIPTION";
    static String DATE = "DATE";
    static String TIME = "TIME";
    public static String MIN_LAT = "MIN_LAT";
    public static String MAX_LAT = "MAX_LAT";
    public static String MIN_LNG = "MIN_LNG";
    public static String MAX_LNG = "MAX_LNG";
}
