package is.ippurpussur.gigfinder.model.parser;

import is.ippurpussur.gigfinder.model.bandsintown.BandsintownEvent;
import is.ippurpussur.gigfinder.model.bandsintown.BandsintownVenue;
import is.ippurpussur.gigfinder.model.neo4j.*;
import is.ippurpussur.gigfinder.neo4j.service.AreaService;
import is.ippurpussur.gigfinder.restclient.googlegeocoder.GeocoderAPIImpl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static is.ippurpussur.gigfinder.model.parser.Key.ADMIN_AREA_LEVEL_1;
import static is.ippurpussur.gigfinder.model.parser.Key.CITY;

public final class BandsintownParser {

    private final DomainParser domainParser;
    private final boolean convertedFromSongkick;

    public BandsintownParser() {
        domainParser = new DomainParser();
        convertedFromSongkick = false;
    }

    private boolean areaMatchesBandsintownArea(final Area area, final String country) {
        return Objects.equals(area.getCountry(), country);
    }

    private Area createAreaFromBandsintownVenue(final BandsintownVenue bandsintownVenue,
                                                final AreaService areaService,
                                                final String languageCode) throws IOException {

        final String country = bandsintownVenue.getCountry();
        final Double latitude = bandsintownVenue.getLatitude();
        final Double longitude = bandsintownVenue.getLongitude();
        final Coordinates coordinates = new Coordinates(latitude, longitude, false);

        final Area areaToUpdate = getAreaToUpdate(latitude, longitude, areaService, country);
        String city = areaToUpdate != null ? areaToUpdate.getCity() : null;
        String adminAreaLevel1 = null;
        final Map<String, String> address = getAddress(areaToUpdate, latitude, longitude, languageCode);

        if (address != null) {
            city = address.get(CITY);
            adminAreaLevel1 = address.get(ADMIN_AREA_LEVEL_1);
        }

        return new Area(coordinates, null, null, city, adminAreaLevel1, country);
    }

    private Area getAreaToUpdate(final Double latitude,
                                 final Double longitude,
                                 final AreaService areaService,
                                 final String country) {

        final Map<String, Double> formattedCoordinates = domainParser.formatCoordinates(latitude, longitude);
        final List<Area> areasToUpdate = areaService.findByFormattedCoordinatesBetween(
                formattedCoordinates.get(Key.LAT), formattedCoordinates.get(Key.LNG));

        for (final Area area : areasToUpdate) {
            if (areaMatchesBandsintownArea(area, country)) {
                return area;
            }
        }
        return null;
    }

    private Map<String, String> getAddress(final Area area,
                                           final Double latitude,
                                           final Double longitude,
                                           final String languageCode) throws IOException {

        String city = area != null ? area.getCity() : null;
        if (city == null || city.trim().isEmpty()) {
            final GeocoderAPIImpl geocoderAPI = new GeocoderAPIImpl();
            final Map<String, String> address = geocoderAPI.getAddress(latitude, longitude, languageCode);
            if (address != null) {
                return address;
            }
        }
        return null;
    }

    private Map<String, Object> createAllPropertiesToSetFromBandsintownVenue(
            final BandsintownVenue bandsintownVenue, final AreaService areaService, final String languageCode)
            throws IOException {

        final Map<String, Object> allPropertiesToSet = new HashMap<>();
        final String name = bandsintownVenue.getName();

        final Area area = createAreaFromBandsintownVenue(bandsintownVenue, areaService, languageCode);
        allPropertiesToSet.put(Key.NAME, name);
        allPropertiesToSet.put(Key.AREA, area);

        return allPropertiesToSet;
    }

    private Venue convertBandsintownVenueToNodeEntity(final BandsintownVenue bandsintownVenue,
                                                      final AreaService areaService,
                                                      final String languageCode) throws IOException {

        final Map<String, Object> allPropertiesToSet = createAllPropertiesToSetFromBandsintownVenue(
                bandsintownVenue, areaService, languageCode);
        return domainParser.setAllPropertiesToVenue(convertedFromSongkick, allPropertiesToSet);
    }

    public Event convertBandsintownEventToNodeEntity(final BandsintownEvent bandsintownEvent,
                                                     final AreaService areaService,
                                                     final String languageCode) throws IOException {

        String eventName = bandsintownEvent.getName();
        final String atSign = "@";
        final String at = "at";
        if (eventName.contains(atSign)) {
            eventName = eventName.replace(atSign, at);
        }

        final Venue venue =
                convertBandsintownVenueToNodeEntity(bandsintownEvent.getVenue(), areaService, languageCode);
        final StartTime startTime = createStartTimeFromBandsintownEvent(bandsintownEvent);

        return new Event(
                null,
                bandsintownEvent.getBandsintownId(),
                eventName,
                bandsintownEvent.getArtists(),
                venue,
                startTime
        );
    }

    private StartTime createStartTimeFromBandsintownEvent(final BandsintownEvent bandsintownEvent) {

        final String dateTime = bandsintownEvent.getDateTime();
        StartTime startTime = null;

        if (dateTime != null) {
            final Map<String, String> dateAndTime = domainParser.splitDateTimeToDateAndTime(dateTime);
            startTime = new StartTime(dateTime, dateAndTime.get(Key.DATE), dateAndTime.get(Key.TIME));
        }

        return startTime;
    }
}
