package is.ippurpussur.gigfinder.model.parser;

import is.ippurpussur.gigfinder.model.neo4j.*;
import is.ippurpussur.gigfinder.model.songkick.*;
import is.ippurpussur.gigfinder.restclient.googlegeocoder.GeocoderAPIImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static is.ippurpussur.gigfinder.model.parser.Key.*;

public final class SongkickParser {

    private final DomainParser domainParser;
    private final boolean convertedFromSongkick;

    public SongkickParser() {
        domainParser = new DomainParser();
        convertedFromSongkick = true;
    }

    private Map<String, String> getCityAndCountryFromSongkickVenue(final SongkickVenue songkickVenue) {
        Map<String, String> cityAndCountry = new HashMap<>();

        String cityName = null;
        String countryName = null;
        final SongkickCity songkickCity = songkickVenue.getCity();

        if (songkickCity != null) {
            cityName = songkickCity.getName();
            countryName = songkickCity.getCountry().getName();
        }

        cityAndCountry.put(CITY, cityName);
        cityAndCountry.put(COUNTRY, countryName);

        return cityAndCountry;
    }

    private Area createAreaFromSongkickVenue(final SongkickVenue songkickVenue, final String languageCode) throws IOException {
        final Double latitude = songkickVenue.getLatitude();
        final Double longitude = songkickVenue.getLongitude();
        final String street = songkickVenue.getStreet();
        final String zip = songkickVenue.getZip();
        final Map<String, String> cityAndCountry = getCityAndCountryFromSongkickVenue(songkickVenue);
        String city = cityAndCountry.get(CITY);
        String adminAreaLevel1 = null;
        final String country = cityAndCountry.get(COUNTRY);
        String formattedCountry = country != null ? formatCountryName(country) : null;

        final Coordinates coordinates;
        final GeocoderAPIImpl geocoderAPI = new GeocoderAPIImpl();

        if (latitude == null || longitude == null) {
            final String address = domainParser.formatAddress(street, zip, city, null, formattedCountry);
            final Map<String, Double> latLng = geocoderAPI.getLatLng(address);
            coordinates = new Coordinates(latLng.get(LAT), latLng.get(LNG), true);
        } else {
            coordinates = new Coordinates(latitude, longitude, true);
        }

        if ((city == null || country == null) && coordinates.getLatLng() != null) {
            Map<String, String> address =
                    geocoderAPI.getAddress(coordinates.getLatitude(), coordinates.getLongitude(), languageCode);
            if (address != null) {
                city = address.get(CITY);
                adminAreaLevel1 = address.get(ADMIN_AREA_LEVEL_1);
                formattedCountry = address.get(COUNTRY);
            }
        }

        return new Area(coordinates, street, zip, city, adminAreaLevel1, formattedCountry);
    }

    private String formatCountryName(final String countryName) {
        String formattedCountryName = countryName;
        if (countryName.equals("UK")) {
            formattedCountryName = "United Kingdom";
        } else if (countryName.equals("US")) {
            formattedCountryName = "United States";
        }

        return formattedCountryName;
    }

    private Map<String, Object> getAllPropertiesToSetFromSongkickVenue(final SongkickVenue songkickVenue, final String languageCode) throws IOException {
        final Map<String, Object> allProperties = new HashMap<>();

        final String venueName = songkickVenue.getName();
        final Area area = createAreaFromSongkickVenue(songkickVenue, languageCode);
        final String website = songkickVenue.getWebsite();
        final String phone = songkickVenue.getPhone();
        final String description = songkickVenue.getDescription();

        allProperties.put(NAME, venueName);
        allProperties.put(AREA, area);
        allProperties.put(WEBSITE, website);
        allProperties.put(PHONE, phone);
        allProperties.put(DESCRIPTION, description);

        return allProperties;
    }

    private Venue convertSongkickVenueToNodeEntity(final SongkickVenue songkickVenue, final String languageCode) throws IOException {
        final Map<String, Object> allPropertiesToSet =
                getAllPropertiesToSetFromSongkickVenue(songkickVenue, languageCode);
        return domainParser.setAllPropertiesToVenue(convertedFromSongkick, allPropertiesToSet);
    }

    private Event convertSongkickEventToNodeEntity(final SongkickEvent songkickEvent, final String languageCode) throws IOException {
        final StartTime songkickStartTime = songkickEvent.getStartTime();
        final StartTime startTimeToSet =
                songkickStartTime != null ? domainParser.createStartTime(songkickStartTime) : null;

        return new Event(
                songkickEvent.getSongkickId(),
                null,
                songkickEvent.getName(),
                convertPerformanceListToArtistList(songkickEvent.getPerformanceList()),
                convertSongkickVenueToNodeEntity(songkickEvent.getVenue(), languageCode),
                startTimeToSet
        );
    }

    public List<Artist> convertPerformanceListToArtistList(final List<Performance> performanceList) {
        final List<Artist> artistList = new ArrayList<>();
        performanceList.forEach(performance -> {
            final Artist artist = convertSongkickArtistToNodeEntity(performance.getArtist());
            artistList.add(artist);
        });

        return artistList;
    }

    private Artist convertSongkickArtistToNodeEntity(final SongkickArtist songkickArtist) {
        final String name = songkickArtist.getName();
        final List<Identifier> identifiers = songkickArtist.getIdentifiers();
        final String mbid = identifiers != null && !identifiers.isEmpty() ? identifiers.get(0).getMbid() : null;

        return new Artist(name, mbid);
    }

    public List<Event> convertSongkickEventsToNodeEntities(final List<SongkickEvent> songkickEvents, final String languageCode) {
        final List<Event> events = new ArrayList<>();

        songkickEvents.forEach(songkickEvent -> {
            Event event = null;
            try {
                event = convertSongkickEventToNodeEntity(songkickEvent, languageCode);
            } catch (IOException e) {
                e.printStackTrace();
            }
            events.add(event);
        });

        return events;
    }
}
