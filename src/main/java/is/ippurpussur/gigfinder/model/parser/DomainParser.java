package is.ippurpussur.gigfinder.model.parser;

import is.ippurpussur.gigfinder.model.lastfm.SimilarArtist;
import is.ippurpussur.gigfinder.model.neo4j.*;
import org.apache.commons.lang3.StringUtils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

public final class DomainParser {

    private DecimalFormat decimalFormat;

    public DomainParser() {
        decimalFormat = new DecimalFormat("###.#");
        decimalFormat.setRoundingMode(RoundingMode.DOWN);
    }

    boolean areasAreSame(final Area area, final Area otherArea) {
        final String city = area.getCity();
        final String otherCity = otherArea.getCity();
        final Long id = area.getId();
        final Long otherId = otherArea.getId();
        final String latLng = area.getLatLng();
        final String otherLatLng = otherArea.getLatLng();

        boolean areasAreSame = false;
        if (stringHasValue(city) || stringHasValue(otherCity)) {
            areasAreSame = Objects.equals(city, otherCity);
        }
        if (latLng != null && otherLatLng != null) {
            areasAreSame = latLng.equals(otherLatLng);
        }
        if (id != null && otherId != null) {
            areasAreSame = id.equals(otherId);
        }

        return areasAreSame;
    }

    private boolean mapValueExists(final String label, final Map<String, Object> properties) {
        return properties.containsKey(label) && properties.get(label) != null;
    }

    Venue setAllPropertiesToVenue(final boolean convertedFromSongkick, final Map<String, Object> allProperties) {
        final String nameToSet =
                mapValueExists(Key.NAME, allProperties) ? (String) allProperties.get(Key.NAME) : null;
        final Area areaToSet =
                mapValueExists(Key.AREA, allProperties) ? (Area) allProperties.get(Key.AREA) : null;
        final String websiteToSet =
                mapValueExists(Key.WEBSITE, allProperties) ? (String) allProperties.get(Key.WEBSITE) : null;
        final String phoneToSet =
                mapValueExists(Key.PHONE, allProperties) ? (String) allProperties.get(Key.PHONE) : null;
        final String descriptionToSet =
                mapValueExists(Key.DESCRIPTION, allProperties) ? (String) allProperties.get(Key.DESCRIPTION) : null;

        return new Venue(nameToSet, areaToSet, websiteToSet, phoneToSet, descriptionToSet, convertedFromSongkick);
    }

    public Artist convertSimilarArtistToArtist(final SimilarArtist similarArtist) {
        final String name = similarArtist.getName();
        final String mbid = similarArtist.getMbid();

        return new Artist(name, mbid);
    }

    public Double formatCoordinate(final Double coordinate) {
        final String format = decimalFormat.format(coordinate);
        return Double.parseDouble(format);
    }

    public Map<String, Double> formatCoordinates(final Double latitude, final Double longitude) {
        final Map<String, Double> formattedCoordinates = new HashMap<>();
        final Double formattedLatitude = formatCoordinate(latitude);
        final Double formattedLongitude = formatCoordinate(longitude);
        formattedCoordinates.put(Key.LAT, formattedLatitude);
        formattedCoordinates.put(Key.LNG, formattedLongitude);

        return formattedCoordinates;
    }

    public Map<String, Double> setAllowableRangeForCoordinates(
            final Double latitude, final Double longitude, final Double errorRange) {
        final Map<String, Double> coordinatesWithinAllowableRange = new HashMap<>();

        if (latitude != null && longitude != null) {
            final Double minLat = latitude - errorRange;
            final Double maxLat = latitude + errorRange;
            final Double minLng = longitude - errorRange;
            final Double maxLng = longitude + errorRange;

            final Map<String, Double> minLatMinLng = formatCoordinates(minLat, minLng);
            final Map<String, Double> maxLatMaxLng = formatCoordinates(maxLat, maxLng);

            coordinatesWithinAllowableRange.put(Key.MIN_LAT, minLatMinLng.get(Key.LAT));
            coordinatesWithinAllowableRange.put(Key.MIN_LNG, minLatMinLng.get(Key.LNG));
            coordinatesWithinAllowableRange.put(Key.MAX_LAT, maxLatMaxLng.get(Key.LAT));
            coordinatesWithinAllowableRange.put(Key.MAX_LNG, maxLatMaxLng.get(Key.LNG));
        }

        return coordinatesWithinAllowableRange;
    }

    Map<String, String> splitDateTimeToDateAndTime(final String dateTime) {
        Map<String, String> dateAndTime = new HashMap<>();

        if (dateTime != null) {
            final String[] split = dateTime.split("T");
            dateAndTime.put(Key.DATE, split[0]);
            dateAndTime.put(Key.TIME, split[1]);
        }

        return dateAndTime;
    }

    private boolean artistsAreSame(final Artist artist, final Artist otherArtist) {
        return Objects.equals(artist.getMbid(), otherArtist.getMbid()) || Objects.equals(artist.getName(), otherArtist.getName());
    }

    private boolean atLeastOneArtistMatches(final List<Artist> artists, final List<Artist> otherArtists) {
        for (final Artist artist : artists) {
            for (final Artist otherArtist : otherArtists) {
                if (artistsAreSame(artist, otherArtist)) {
                    return true;
                }
            }
        }
        return false;
    }

    boolean startTimeMatches(final StartTime startTime, final StartTime otherStartTime) {
        if (startTime != null && otherStartTime != null) {
            final String time = startTime.getTime();
            final String otherTime = otherStartTime.getTime();
            final boolean timeMatches = Objects.equals(time, otherTime);
            final boolean dateMatches = Objects.equals(startTime.getDate(), otherStartTime.getDate());
            final String timeZero = "00:00:00";
            final boolean isTimeZeroAndNull =
                    (time == null && timeZero.equals(otherTime)) || (timeZero.equals(time) && otherTime == null);

            if (isTimeZeroAndNull || time == null || otherTime == null) {
                return dateMatches;
            }
            return timeMatches && dateMatches;
        }
        return false;
    }

    boolean eventsAreSame(final Event event, final Event otherEvent) {
        final List<Artist> artists = event.getArtists();
        final List<Artist> eventArtists = artists != null ? artists : new ArrayList<>();

        final List<Artist> otherArtists = otherEvent.getArtists();
        final List<Artist> otherEventArtists = otherArtists != null ? otherArtists : new ArrayList<>();

        final Venue venue = event.getVenue();
        final Venue otherVenue = otherEvent.getVenue();

        final StartTime startTime = event.getStartTime();
        final StartTime otherStartTime = otherEvent.getStartTime();
        boolean startTimesHaveValues = startTime != null && otherStartTime != null;
        boolean startTimeMatches = false;
        if (startTimesHaveValues) {
            if (event.getSongkickId() != null && otherEvent.getBandsintownId() != null) {
                startTimeMatches = Objects.equals(startTime.getDate(), otherStartTime.getDate());
            } else {
                startTimeMatches = startTimeMatches(startTime, otherStartTime);
            }
        }

        boolean eventsAreSame;
        final boolean atLeastOneArtistMatches = atLeastOneArtistMatches(eventArtists, otherEventArtists);
        final boolean venuesAreSame = venuesAreSame(venue, otherVenue);

        if (startTimesHaveValues) {
            eventsAreSame = atLeastOneArtistMatches && startTimeMatches;
        } else {
            eventsAreSame = atLeastOneArtistMatches && venuesAreSame;
        }

        return eventsAreSame;
    }

    boolean coordinatesHaveValues(final Coordinates coordinates) {
        return coordinates != null
                && coordinates.getLatitude() != null
                && coordinates.getLongitude() != null
                && coordinates.getLatitude() != 0
                && coordinates.getLongitude() != 0;
    }

    public boolean stringHasValue(final String string) {
        return string != null && !string.trim().isEmpty();
    }

    private boolean venueNameMatch(final String venueName, final String otherVenueName) {
        final String formattedVenueName =
                stringHasValue(venueName) ? StringUtils.stripAccents(venueName).toLowerCase() : null;
        final String formattedOtherVenueName =
                stringHasValue(otherVenueName) ? StringUtils.stripAccents(otherVenueName).toLowerCase() : null;
        boolean venueNameMatch = false;

        if (Objects.equals(formattedVenueName, formattedOtherVenueName)) {
            venueNameMatch = true;
        } else if (formattedVenueName != null && formattedOtherVenueName != null) {
            venueNameMatch =
                    formattedVenueName.contains(formattedOtherVenueName) || formattedOtherVenueName.contains(formattedVenueName);
        }

        return venueNameMatch;
    }

    private boolean venuesAreInSameCity(final Venue venue, final Venue otherVenue) {
        final Area area = venue.getArea();
        final Area otherArea = otherVenue.getArea();
        final String city = area != null ? area.getCity() : null;
        final String otherCity = otherArea != null ? otherArea.getCity() : null;

        return Objects.equals(city, otherCity);
    }

    private boolean venuesAreLocatedInSameArea(final Venue venue, final Venue otherVenue) {
        final String latLng = venue.getLatLng();
        final String otherLatLng = otherVenue.getLatLng();
        final Area area = venue.getArea();
        final Area otherArea = otherVenue.getArea();

        if (latLng != null && otherLatLng != null) {
            return latLng.equals(otherLatLng);
        } else if (area != null && otherArea != null) {
            return areasAreSame(venue.getArea(), otherVenue.getArea());
        }
        return false;
    }

    private boolean zipMatches(final Area area, final Area otherArea) {
        final String zip = area != null ? area.getZip() : null;
        final String otherZip = otherArea != null ? otherArea.getZip() : null;
        final boolean zipsAreSame = (stringHasValue(zip) && stringHasValue(otherZip)) && zip.equals(otherZip);

        final String country = area != null ? area.getCountry() : null;
        final String otherCountry = otherArea != null ? otherArea.getCountry() : null;
        final boolean countriesAreSame = (stringHasValue(country) && stringHasValue(otherCountry)) && country.equals(otherCountry);

        return countriesAreSame && zipsAreSame;
    }

    public boolean venuesAreSame(final Venue venue, final Venue otherVenue) {
        final Long id = venue != null ? venue.getId() : null;
        final Long otherId = otherVenue != null ? otherVenue.getId() : null;
        boolean venueMatches = false;

        if (id != null && otherId != null) {
            venueMatches = id.equals(otherId);
        } else if (venue != null && otherVenue != null) {
            venueMatches = venuesAreInSameCity(venue, otherVenue);

            final String venueName = venue.getName();
            final String otherVenueName = otherVenue.getName();
            final Area area = venue.getArea();
            final Area otherArea = otherVenue.getArea();
            final String zip = area != null ? area.getZip() : null;
            final String otherZip = otherArea != null ? otherArea.getZip() : null;

            if (venueMatches && stringHasValue(venueName) && stringHasValue(otherVenueName)) {
                venueMatches = venueNameMatch(venueName, otherVenueName);
            }
            if (venueMatches && stringHasValue(zip) && stringHasValue(otherZip)) {
                venueMatches = zipMatches(area, otherArea);
            }
            if (venueMatches && area != null && otherArea != null) {
                venueMatches = venuesAreLocatedInSameArea(venue, otherVenue);
            }
        }

        return venueMatches;
    }

    public String simplifyEventName(final String eventName) {
        String simpleEventName = eventName;
        final String with = "with";
        final String at = "at";
        final String in = "in";
        final String leftBracket = Pattern.quote("(");

        if (eventName.contains(with)) {
            simpleEventName = eventName.split(with)[0].trim();
        }
        if (simpleEventName.contains(at)) {
            simpleEventName = eventName.split(at)[0].trim();
        }
        if (simpleEventName.contains(in)) {
            simpleEventName = simpleEventName.split(in)[0].trim();
        } else if (simpleEventName.contains(leftBracket)) {
            simpleEventName = simpleEventName.split(leftBracket)[0].trim();
        }
        return simpleEventName;
    }

    private StartTime simplifyStartTime(final StartTime startTime) {
        final String dateTime = startTime.getDateTime();
        final String date = startTime.getDate();
        final String time = startTime.getTime();

        final String plus = "+";
        final String plusRegex = Pattern.quote(plus);

        final String newDateTime = (dateTime != null && dateTime.contains(plus)) ? dateTime.split(plusRegex)[0] : null;
        final String newTime = (time != null && time.contains(plus)) ? time.split(plusRegex)[0] : null;

        return new StartTime(newDateTime, date, newTime);
    }

    StartTime createStartTime(final StartTime startTime) {
        final String dateTime = startTime.getDateTime();
        if (dateTime != null) {
            final Map<String, String> dateAndTime = splitDateTimeToDateAndTime(dateTime);
            startTime.setDate(dateAndTime.get(Key.DATE));
            startTime.setTime(dateAndTime.get(Key.TIME));
        }
        return simplifyStartTime(startTime);
    }

    public String formatAddress(
            final String street, final String zip, final String city, final String adminAreaLevel1, final String country) {
        final String comma = ", ";

        final String streetPlusComma = street != null ? street + comma : "";
        final String zipPlusComma = zip != null ? zip + comma : "";
        final String cityPlusComma = city != null ? city + comma : "";
        final String adminAreaLevel1PlusComma = adminAreaLevel1 != null ? adminAreaLevel1 + comma : "";
        final String countryPlusComma = country != null ? country + comma : "";

        return streetPlusComma + zipPlusComma + cityPlusComma + adminAreaLevel1PlusComma + countryPlusComma;
    }

    public String formatDateTime(final String date, final String time, final String dateTime) {
        final String dateZero = "0000-00-00";
        final String timeZero = "00:00:00";
        final String dateForDateTime = date != null ? date : dateZero;
        final String timeForDateTime = time != null ? time : timeZero;

        return dateTime != null ? dateTime : dateForDateTime + "T" + timeForDateTime;
    }
}
