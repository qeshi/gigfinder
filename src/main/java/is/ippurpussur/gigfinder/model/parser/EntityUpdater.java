package is.ippurpussur.gigfinder.model.parser;

import is.ippurpussur.gigfinder.model.neo4j.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public final class EntityUpdater {

    private final DomainParser domainParser;

    public EntityUpdater() {
        domainParser = new DomainParser();
    }

    public Area findAreaToUpdate(final List<Area> areasToCompare, final Area area) {
        for (final Area areaToCompare : areasToCompare) {
            if (domainParser.areasAreSame(area, areaToCompare)) {
                return areaToCompare;
            }
        }
        return null;
    }

    public Event findEventToUpdate(final List<Event> events, final Event eventToCreate) {
        for (final Event event : events) {
            if (domainParser.eventsAreSame(event, eventToCreate)) {
                return event;
            }
        }
        return null;
    }

    public StartTime findStartTimeToUpdate(final List<StartTime> startTimes, final StartTime startTimeToCreate) {
        for (final StartTime startTime : startTimes) {
            if (domainParser.startTimeMatches(startTime, startTimeToCreate)) {
                return startTime;
            }
        }
        return null;
    }

    public Artist findArtistToUpdate(
            final List<Artist> existingArtists, final Artist artistToCreate, final Map<Artist, Integer> numberOfEvents) {
        for (final Artist existingArtist : existingArtists) {
            for (final Map.Entry<Artist, Integer> numberOfEvent : numberOfEvents.entrySet()) {
                final Artist artistAsKey = numberOfEvent.getKey();
                final boolean artistHasMultipleUpcomingEvents = numberOfEvent.getValue() >= 2;
                if (existingArtist.equals(artistAsKey)
                        && (!artistHasMultipleUpcomingEvents
                        || Objects.equals(existingArtist.getWebsite(), artistToCreate.getWebsite()))) {
                    return existingArtist;
                }
            }
        }
        return null;
    }

    public Area updateArea(final Area areaToUpdate, final Area newArea, final Coordinates coordinates) {
        final Area areaToUpdateCopy = new Area(areaToUpdate);
        final String street = newArea.getStreet();
        final String city = newArea.getCity();
        final String adminAreaLevel1 = newArea.getAdminAreaLevel1();
        final String country = newArea.getCountry();
        final String zip = newArea.getZip();
        final String latLng = coordinates != null ? coordinates.getLatLng() : null;

        areaToUpdateCopy.setCoordinates(coordinates);
        areaToUpdateCopy.setStreet(domainParser.stringHasValue(street) ? street : areaToUpdate.getStreet());
        areaToUpdateCopy.setCity(domainParser.stringHasValue(city) ? city : areaToUpdate.getCity());
        areaToUpdateCopy.setAdminAreaLevel1(
                domainParser.stringHasValue(adminAreaLevel1) ? adminAreaLevel1 : areaToUpdate.getAdminAreaLevel1());
        areaToUpdateCopy.setCountry(domainParser.stringHasValue(country) ? country : areaToUpdate.getCountry());
        areaToUpdateCopy.setZip(domainParser.stringHasValue(zip) ? zip : areaToUpdate.getZip());
        areaToUpdateCopy.setLatLng(domainParser.stringHasValue(latLng) ? latLng : areaToUpdate.getLatLng());

        final String address = domainParser.formatAddress(
                areaToUpdateCopy.getStreet(),
                areaToUpdateCopy.getZip(),
                areaToUpdateCopy.getCity(),
                areaToUpdateCopy.getAdminAreaLevel1(),
                areaToUpdateCopy.getCountry()
        );
        areaToUpdateCopy.setAddress(address);

        return areaToUpdateCopy;
    }

    public StartTime updateStartTime(final StartTime startTimeToUpdate, final StartTime startTimeToCompare) {
        final StartTime startTimeToUpdateCopy = new StartTime(startTimeToUpdate);
        final String dateTimeToSet = startTimeToCompare.getDateTime();

        if (startTimeToCompare.getDateTime() != null) {
            startTimeToUpdateCopy.setDateTime(dateTimeToSet);
            final Map<String, String> dateAndTime = domainParser.splitDateTimeToDateAndTime(dateTimeToSet);

            startTimeToUpdateCopy.setDate(dateAndTime.get(Key.DATE));
            startTimeToUpdateCopy.setTime(dateAndTime.get(Key.TIME));
        } else {
            final String dateToSet = startTimeToCompare.getDate();
            final String timeToSet = startTimeToCompare.getTime();

            if (dateToSet != null) {
                startTimeToUpdateCopy.setDate(dateToSet);
            }
            if (timeToSet != null) {
                startTimeToUpdateCopy.setTime(timeToSet);
            }
        }
        return startTimeToUpdateCopy;
    }

    public Coordinates updateCoordinates(final Coordinates coordinatesToUpdate, final Coordinates newCoordinates) {
        final Coordinates coordinatesToUpdateCopy = new Coordinates(coordinatesToUpdate);
        final Double newLatitude = newCoordinates.getLatitude();
        final Double newLongitude = newCoordinates.getLongitude();
        final Double latitudeToSet = newLatitude != null ? newLatitude : coordinatesToUpdate.getLatitude();
        final Double longitudeToSet = newLongitude != null ? newLongitude : coordinatesToUpdate.getLongitude();

        if (newCoordinates.isCreatedFromSongkick() || !domainParser.coordinatesHaveValues(coordinatesToUpdateCopy)) {
            coordinatesToUpdateCopy.setLatitude(latitudeToSet);
            coordinatesToUpdateCopy.setLongitude(longitudeToSet);
        }

        return coordinatesToUpdateCopy;
    }

    public Venue updateVenue(final Venue venueToUpdate, final Venue newVenue, final Area area) {
        final Venue venueToUpdateCopy = new Venue(venueToUpdate);
        final String nameToSet = newVenue.getName();
        final String websiteToSet = newVenue.getWebsite();
        final String phoneToSet = newVenue.getPhone();
        final String descriptionToSet = newVenue.getDescription();
        final String nameToUpdate = venueToUpdate.getName();
        final Area areaToUpdate = venueToUpdate.getArea();

        final Area areaToSet;
        if (areaToUpdate != null) {
            final Coordinates coordinatesToUpdate = areaToUpdate.getCoordinates();
            final Coordinates coordinatesToSet = area != null ? area.getCoordinates() : coordinatesToUpdate;
            areaToSet = area != null ? updateArea(areaToUpdate, area, coordinatesToSet) : areaToUpdate;
        } else {
            areaToSet = area;
        }

        if (newVenue.isCreatedFromSongkick() || !domainParser.stringHasValue(nameToUpdate)) {
            venueToUpdateCopy.setName(domainParser.stringHasValue(nameToSet) ? nameToSet : nameToUpdate);
        }
        if (newVenue.isCreatedFromSongkick() && areaToSet != null) {
            venueToUpdateCopy.setLatLng(areaToSet.getLatLng());
        }

        venueToUpdateCopy.setArea(areaToSet);
        venueToUpdateCopy.setWebsite(domainParser.stringHasValue(websiteToSet) ? websiteToSet : venueToUpdate.getWebsite());
        venueToUpdateCopy.setPhone(domainParser.stringHasValue(phoneToSet) ? phoneToSet : venueToUpdate.getPhone());
        venueToUpdateCopy.setDescription(
                domainParser.stringHasValue(descriptionToSet) ? descriptionToSet : venueToUpdate.getDescription());

        return venueToUpdateCopy;
    }

    public Event updateEvent(final Event eventToUpdate,
                             final Event newEvent,
                             final List<Artist> artistsToSet,
                             final Venue venueToSet,
                             final StartTime startTimeToSet) {
        final Event eventToUpdateCopy = new Event(eventToUpdate);
        final Long songkickIdToSet = newEvent.getSongkickId();
        final Long bandsintownIdToSet = newEvent.getBandsintownId();
        final String nameToSet = newEvent.getName();
        final String nameToUpdate = eventToUpdate.getName();

        eventToUpdateCopy.setSongkickId(songkickIdToSet != null ? songkickIdToSet : eventToUpdate.getSongkickId());
        eventToUpdateCopy.setBandsintownId(bandsintownIdToSet != null ? bandsintownIdToSet : eventToUpdate.getBandsintownId());
        eventToUpdateCopy.setVenue(venueToSet != null ? venueToSet : eventToUpdate.getVenue());

        if (!domainParser.stringHasValue(nameToUpdate)) {
            eventToUpdateCopy.setName(domainParser.stringHasValue(nameToSet) ? nameToSet : nameToUpdate);
        }
        if (!existingEventIsCreatedFromSongkickAndNewEventIsFromBandsintown(eventToUpdate, newEvent)) {
            eventToUpdateCopy.setStartTime(startTimeToSet != null ? startTimeToSet : eventToUpdate.getStartTime());
            eventToUpdateCopy.setArtists(artistsToSet);
        }
        return eventToUpdateCopy;
    }

    private boolean existingEventIsCreatedFromSongkickAndNewEventIsFromBandsintown(
            final Event existingEvent, final Event newEvent) {
        return existingEvent.getSongkickId() != null && newEvent.getBandsintownId() != null;
    }
}
