package is.ippurpussur.gigfinder.model.webserviceresponse;

import is.ippurpussur.gigfinder.model.neo4j.Event;

import java.util.List;

public final class EventsResult {

    private List<Event> events;
    private int pageNumber;
    private int rangeStart;
    private int rangeEnd;
    private int hits;
    private long fetchTimeInMillis;

    public EventsResult(final List<Event> events,
                        final int pageNumber,
                        final int numberOfResultsPerPage,
                        final int numberOfTotalEvents) {
        this.events = events;
        this.pageNumber = pageNumber;
        final int pagesToSkip = pageNumber - 1;

        final int rangeStartTemp = pagesToSkip * numberOfResultsPerPage + 1;
        calculateRangeStart(numberOfTotalEvents, rangeStartTemp);

        final int rangeEndTemp = rangeStartTemp + numberOfResultsPerPage - 1;
        calculateRangeEnd(numberOfTotalEvents, numberOfResultsPerPage, rangeEndTemp);

        this.hits = numberOfTotalEvents;
        fetchTimeInMillis = System.currentTimeMillis();
    }

    public List<Event> getEvents() {
        return events;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getRangeStart() {
        return rangeStart;
    }

    public int getRangeEnd() {
        return rangeEnd;
    }

    public int getHits() {
        return hits;
    }

    public long getFetchTimeInMillis() {
        return fetchTimeInMillis;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setRangeStart(int rangeStart) {
        this.rangeStart = rangeStart;
    }

    public void setRangeEnd(int rangeEnd) {
        this.rangeEnd = rangeEnd;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public void setFetchTimeInMillis(long fetchTimeInMillis) {
        this.fetchTimeInMillis = fetchTimeInMillis;
    }

    private void calculateRangeStart(final int numberOfTotalEvents, final int rangeStart) {
        if (numberOfTotalEvents < rangeStart) {
            this.rangeStart = 0;
        } else {
            this.rangeStart = rangeStart;
        }
    }

    private void calculateRangeEnd(final int numberOfTotalEvents, final int numberOfResultsPerPage, final int rangeEnd) {
        if (numberOfTotalEvents + numberOfResultsPerPage <= rangeEnd) {
            this.rangeEnd = 0;
        } else if (numberOfTotalEvents <= rangeEnd) {
            this.rangeEnd = numberOfTotalEvents;
        } else {
            this.rangeEnd = rangeEnd;
        }
    }

    @Override
    public String toString() {
        return "EventsResult{" +
                "events=" + events +
                ", pageNumber=" + pageNumber +
                ", rangeStart=" + rangeStart +
                ", rangeEnd=" + rangeEnd +
                ", hits=" + hits +
                ", fetchTimeInMillis=" + fetchTimeInMillis +
                '}';
    }
}
