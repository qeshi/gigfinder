package is.ippurpussur.gigfinder.model.songkick;

public final class Performance {

    private final SongkickArtist artist;

    public Performance(final SongkickArtist artist) {
        this.artist = artist;
    }

    public SongkickArtist getArtist() {
        return artist;
    }

    @Override
    public String toString() {
        return "Performance{" +
                "artist=" + artist +
                '}';
    }
}
