package is.ippurpussur.gigfinder.model.songkick;

import com.google.gson.annotations.SerializedName;

public final class SongkickVenue {

    @SerializedName("id")
    private final Long songkickId;

    @SerializedName("displayName")
    private final String name;

    @SerializedName("lat")
    private final Double latitude;

    @SerializedName("lng")
    private final Double longitude;

    private String street;

    private String zip;

    private SongkickCity city;

    private String website;

    private String phone;

    private String description;

    public SongkickVenue(final Long songkickId,
                         final String name,
                         final Double latitude,
                         final Double longitude) {
        this.songkickId = songkickId;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public SongkickVenue(final Long songkickId,
                         final String name,
                         final Double latitude,
                         final Double longitude,
                         final String street,
                         final String zip,
                         final SongkickCity city,
                         final String website,
                         final String phone,
                         final String description) {
        this.songkickId = songkickId;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.street = street;
        this.zip = zip;
        this.city = city;
        this.website = website;
        this.phone = phone;
        this.description = description;
    }

    public Long getSongkickId() {
        return songkickId;
    }

    public String getName() {
        return name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getStreet() {
        return street;
    }

    public String getZip() {
        return zip;
    }

    public SongkickCity getCity() {
        return city;
    }

    public String getWebsite() {
        return website;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "SongkickVenue{" +
                "songkickId=" + songkickId +
                ", name='" + name + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", street='" + street + '\'' +
                ", zip='" + zip + '\'' +
                ", city=" + city +
                ", website='" + website + '\'' +
                ", phone='" + phone + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
