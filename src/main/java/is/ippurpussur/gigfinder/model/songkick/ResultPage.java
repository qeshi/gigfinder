package is.ippurpussur.gigfinder.model.songkick;

public final class ResultPage {

    private Results results;

    public Results getResults() {
        return results;
    }

    @Override
    public String toString() {
        return "ResultPage{" +
                "results=" + results +
                '}';
    }
}
