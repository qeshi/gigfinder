package is.ippurpussur.gigfinder.model.songkick;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class SongkickArtist {

    @SerializedName("displayName")
    private final String name;

    @SerializedName("identifier")
    private final List<Identifier> identifiers;

    public SongkickArtist(final String name, final List<Identifier> identifiers) {
        this.name = name;
        this.identifiers = identifiers;
    }

    public String getName() {
        return name;
    }

    public List<Identifier> getIdentifiers() {
        return identifiers;
    }

    @Override
    public String toString() {
        return "SongkickArtist{" +
                "name='" + name + '\'' +
                ", identifiers=" + identifiers +
                '}';
    }
}
