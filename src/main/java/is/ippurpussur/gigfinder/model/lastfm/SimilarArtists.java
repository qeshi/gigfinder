package is.ippurpussur.gigfinder.model.lastfm;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class SimilarArtists {

    @SerializedName("artist")
    private List<SimilarArtist> similarArtists;

    public List<SimilarArtist> getSimilarArtists() {
        return similarArtists;
    }

    @Override
    public String toString() {
        return "SimilarArtists{" +
                "similarArtists=" + similarArtists +
                '}';
    }
}
