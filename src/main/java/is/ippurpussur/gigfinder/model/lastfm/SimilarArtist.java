package is.ippurpussur.gigfinder.model.lastfm;

import com.google.gson.annotations.SerializedName;

public final class SimilarArtist {

    private String name;

    private String mbid;

    @SerializedName("match")
    private Double matchLevel;

    public SimilarArtist(final String name, final String mbid, final Double matchLevel) {
        this.name = name;
        this.mbid = mbid;
        this.matchLevel = matchLevel;
    }

    public String getName() {
        return name;
    }

    public String getMbid() {
        return mbid;
    }

    public Double getMatchLevel() {
        return matchLevel;
    }

    @Override
    public String toString() {
        return "SimilarArtist{" +
                "name='" + name + '\'' +
                ", mbid='" + mbid + '\'' +
                ", matchLevel=" + matchLevel +
                '}';
    }
}
