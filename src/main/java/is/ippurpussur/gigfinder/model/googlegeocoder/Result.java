package is.ippurpussur.gigfinder.model.googlegeocoder;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class Result {

    @SerializedName("address_components")
    private List<AddressComponent> addressComponents;

    private Geometry geometry;

    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    @Override
    public String toString() {
        return "Result{" +
                "addressComponents=" + addressComponents +
                ", geometry=" + geometry +
                '}';
    }
}
