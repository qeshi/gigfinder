package is.ippurpussur.gigfinder.model.googlegeocoder;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class AddressComponent {

    @SerializedName("long_name")
    private String name;

    private List<String> types;

    public String getName() {
        return name;
    }

    public List<String> getTypes() {
        return types;
    }

    @Override
    public String toString() {
        return "AddressComponent{" +
                "name='" + name + '\'' +
                ", types=" + types +
                '}';
    }
}
