package is.ippurpussur.gigfinder.model.googlegeocoder;

public final class Geometry {

    private Location location;

    public Location getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "Geometry{" +
                "location=" + location +
                '}';
    }
}
