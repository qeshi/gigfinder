package is.ippurpussur.gigfinder.model.neo4j;

import com.fasterxml.jackson.annotation.JsonIgnore;
import is.ippurpussur.gigfinder.model.parser.DomainParser;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Transient;

import java.util.Objects;

@NodeEntity
public class Area extends AbstractEntity {

    private Coordinates coordinates;

    private String street;

    private String zip;

    private String city;

    private String adminAreaLevel1;

    private String country;

    @JsonIgnore
    private String latLng;

    private String address;

    @Transient
    @JsonIgnore
    private DomainParser domainParser;

    private Area() {
    }

    public Area(final Coordinates coordinates,
                final String street,
                final String zip,
                final String city,
                final String adminAreaLevel1,
                final String country) {
        this.coordinates = coordinates;
        this.street = street == null || street.isEmpty() ? null : street;
        this.zip = zip == null || zip.isEmpty() ? null : zip;
        this.city = city == null || city.isEmpty() ? null : city;
        this.adminAreaLevel1 = adminAreaLevel1 == null || adminAreaLevel1.isEmpty() ? null : adminAreaLevel1;
        this.country = country == null || country.isEmpty() ? null : country;
        latLng = coordinates != null ? coordinates.getLatLng() : null;
        domainParser = new DomainParser();
        address = domainParser.formatAddress(street, zip, city, adminAreaLevel1, country);
    }

    public Area(final Area another) {
        id = another.id;
        uuid = another.uuid;
        coordinates = another.coordinates;
        street = another.street;
        zip = another.zip;
        city = another.city;
        adminAreaLevel1 = another.adminAreaLevel1;
        country = another.country;
        latLng = another.latLng;
        domainParser = new DomainParser();
        address = domainParser.formatAddress(
                another.street, another.zip, another.city, another.adminAreaLevel1, another.country);
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public String getStreet() {
        return street;
    }

    public String getZip() {
        return zip;
    }

    public String getCity() {
        return city;
    }

    public String getAdminAreaLevel1() {
        return adminAreaLevel1;
    }

    public String getCountry() {
        return country;
    }

    public String getLatLng() {
        return latLng;
    }

    public String getAddress() {
        return address;
    }

    public void setCoordinates(final Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public void setZip(final String zip) {
        this.zip = zip;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public void setAdminAreaLevel1(final String adminAreaLevel1) {
        this.adminAreaLevel1 = adminAreaLevel1;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public void setLatLng(String latLng) {
        this.latLng = latLng;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other instanceof Area) {
            final Area otherArea = (Area) other;
            return Objects.equals(uuid, otherArea.uuid)
                    && Objects.equals(coordinates, otherArea.coordinates)
                    && Objects.equals(street, otherArea.street)
                    && Objects.equals(zip, otherArea.zip)
                    && Objects.equals(city, otherArea.city)
                    && Objects.equals(adminAreaLevel1, otherArea.adminAreaLevel1)
                    && Objects.equals(country, otherArea.country);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result += (uuid != null ? uuid.hashCode() : 0) * 37;
        result += (coordinates != null ? coordinates.hashCode() : 0) * 37;
        result += (street != null ? street.hashCode() : 0) * 37;
        result += (zip != null ? zip.hashCode() : 0) * 37;
        result += (city != null ? city.hashCode() : 0) * 37;
        result += (adminAreaLevel1 != null ? adminAreaLevel1.hashCode() : 0) * 37;
        result += (country != null ? country.hashCode() : 0) * 37;

        return result;
    }

    @Override
    public String toString() {
        return "Area{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", coordinates=" + coordinates +
                ", street='" + street + '\'' +
                ", zip='" + zip + '\'' +
                ", city='" + city + '\'' +
                ", adminAreaLevel1='" + adminAreaLevel1 + '\'' +
                ", country='" + country + '\'' +
                ", latLng='" + latLng + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
