package is.ippurpussur.gigfinder.model.neo4j;

import com.fasterxml.jackson.annotation.JsonIgnore;
import is.ippurpussur.gigfinder.model.parser.DomainParser;
import is.ippurpussur.gigfinder.model.parser.Key;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Transient;

import java.util.Map;
import java.util.Objects;

@NodeEntity
public class Coordinates extends AbstractEntity {

    private Double latitude;

    private Double longitude;

    private Double formattedLatitude;

    private Double formattedLongitude;

    @JsonIgnore
    private String latLng;

    @JsonIgnore
    @Transient
    private boolean createdFromSongkick;

    private Coordinates() {
    }

    public Coordinates(final Double latitude, final Double longitude, final boolean createdFromSongkick) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdFromSongkick = createdFromSongkick;

        if (latitude != null && longitude != null) {
            final DomainParser domainParser = new DomainParser();
            final Map<String, Double> formattedLatLng = domainParser.formatCoordinates(latitude, longitude);
            formattedLatitude = formattedLatLng.get(Key.LAT);
            formattedLongitude = formattedLatLng.get(Key.LNG);
            latLng = String.valueOf(latitude) + ',' + String.valueOf(longitude);
        }
    }

    public Coordinates(final Coordinates another) {
        id = another.id;
        uuid = another.uuid;
        latitude = another.latitude;
        longitude = another.longitude;
        formattedLatitude = another.formattedLatitude;
        formattedLongitude = another.formattedLongitude;
        latLng = another.latLng;
        createdFromSongkick = another.createdFromSongkick;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getFormattedLatitude() {
        return formattedLatitude;
    }

    public Double getFormattedLongitude() {
        return formattedLongitude;
    }

    public String getLatLng() {
        return latLng;
    }

    public boolean isCreatedFromSongkick() {
        return createdFromSongkick;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
        if (latitude != null) {
            DomainParser domainParser = new DomainParser();
            formattedLatitude = domainParser.formatCoordinate(latitude);
        }
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
        if (longitude != null) {
            DomainParser domainParser = new DomainParser();
            formattedLongitude = domainParser.formatCoordinate(longitude);
        }
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other instanceof Coordinates) {
            final Coordinates otherCoordinates = (Coordinates) other;
            return Objects.equals(uuid, otherCoordinates.uuid)
                    && Objects.equals(latitude, otherCoordinates.latitude)
                    && Objects.equals(longitude, otherCoordinates.longitude)
                    && Objects.equals(formattedLatitude, otherCoordinates.formattedLatitude)
                    && Objects.equals(formattedLongitude, otherCoordinates.formattedLongitude);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result += (uuid != null ? uuid.hashCode() : 0) * 37;
        result += (latitude != null ? latitude.hashCode() : 0) * 37;
        result += (longitude != null ? longitude.hashCode() : 0) * 37;
        result += (formattedLatitude != null ? formattedLongitude.hashCode() : 0) * 37;
        result += (formattedLongitude != null ? formattedLongitude.hashCode() : 0) * 37;

        return result;
    }

    @Override
    public String toString() {
        return "Coordinates{" +
                "id=" + id +
                ", uuid=" + uuid + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", formattedLatitude='" + formattedLatitude + '\'' +
                ", formattedLongitude='" + formattedLongitude + '\'' +
                ", createdFromSongkick=" + createdFromSongkick +
                '}';
    }
}
