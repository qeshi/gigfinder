package is.ippurpussur.gigfinder.model.neo4j;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.Transient;

import java.util.Objects;

@NodeEntity
public class Venue extends AbstractEntity {

    private String name;

    @Relationship(type = "LOCATED_IN")
    private Area area;

    private String website;

    private String phone;

    private String description;

    @JsonIgnore
    private String latLng;

    @JsonIgnore
    @Transient
    private boolean createdFromSongkick;

    private Venue() {
    }

    public Venue(final String name,
                 final Area area,
                 final String website,
                 final String phone,
                 final String description,
                 final boolean createdFromSongkick) {
        this.name = name;
        this.area = area;
        this.website = website;
        this.phone = phone;
        this.description = description;
        this.createdFromSongkick = createdFromSongkick;
        latLng = area.getLatLng();
    }

    public Venue(final Venue another) {
        id = another.id;
        uuid = another.uuid;
        name = another.name;
        area = another.area;
        website = another.website;
        phone = another.phone;
        description = another.description;
        createdFromSongkick = another.createdFromSongkick;
        latLng = another.latLng;
    }

    public String getName() {
        return name;
    }

    public Area getArea() {
        return area;
    }

    public String getWebsite() {
        return website;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }

    public String getLatLng() {
        return latLng;
    }

    public boolean isCreatedFromSongkick() {
        return createdFromSongkick;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setArea(final Area area) {
        this.area = area;
        if(area != null){
            latLng = area.getLatLng();
        }
    }

    public void setWebsite(final String website) {
        this.website = website;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setLatLng(final String latLng) {
        this.latLng = latLng;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other instanceof Venue) {
            final Venue otherVenue = (Venue) other;
            return Objects.equals(uuid, otherVenue.uuid)
                    && Objects.equals(name, otherVenue.name)
                    && Objects.equals(area, otherVenue.area);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result += (uuid != null ? uuid.hashCode() : 0) * 37;
        result += (name != null ? name.hashCode() : 0) * 37;
        result += (area != null ? area.hashCode() : 0) * 37;

        return result;
    }

    @Override
    public String toString() {
        return "Venue{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", name='" + name + '\'' +
                ", area=" + area +
                ", website='" + website + '\'' +
                ", phone='" + phone + '\'' +
                ", description='" + description + '\'' +
                ", latLng='" + latLng + '\'' +
                ", createdFromSongkick=" + createdFromSongkick +
                '}';
    }
}
