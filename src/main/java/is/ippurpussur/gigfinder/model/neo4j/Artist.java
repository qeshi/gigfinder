package is.ippurpussur.gigfinder.model.neo4j;

import org.neo4j.ogm.annotation.NodeEntity;

import java.util.Objects;

@NodeEntity
public class Artist extends AbstractEntity {

    private String name;

    private String mbid;

    private String website;

    private Artist() {
    }

    public Artist(final String name, final String mbid) {
        this.name = name;
        this.mbid = mbid;
    }

    public Artist(final Artist another) {
        this.id = another.id;
        this.uuid = another.uuid;
        this.name = another.name;
        this.mbid = another.mbid;
        this.website = another.website;
    }

    public String getName() {
        return name;
    }

    public String getMbid() {
        return mbid;
    }

    public String getWebsite() {
        return website;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setMbid(final String mbid) {
        this.mbid = mbid;
    }

    public void setWebsite(final String website) {
        this.website = website;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other instanceof Artist) {
            final Artist otherArtist = (Artist) other;
            return Objects.equals(uuid, otherArtist.uuid)
                    && Objects.equals(name, otherArtist.name)
                    && Objects.equals(mbid, otherArtist.mbid);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result += (uuid != null ? uuid.hashCode() : 0) * 37;
        result += (name != null ? name.hashCode() : 0) * 37;
        result += (mbid != null ? mbid.hashCode() : 0) * 37;

        return result;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", name='" + name + '\'' +
                ", mbid='" + mbid + '\'' +
                ", website='" + website + '\'' +
                '}';
    }
}
