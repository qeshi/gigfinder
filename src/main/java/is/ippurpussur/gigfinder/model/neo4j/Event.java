package is.ippurpussur.gigfinder.model.neo4j;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;
import java.util.Objects;

@NodeEntity
public class Event extends AbstractEntity {

    @JsonIgnore
    private Long songkickId;

    @JsonIgnore
    private Long bandsintownId;

    private String name;

    @Relationship(type = "PERFORMS", direction = Relationship.INCOMING)
    private List<Artist> artists;

    @Relationship(type = "HELD_IN")
    private Venue venue;

    @Relationship(type = "STARTS_AT")
    private StartTime startTime;

    private Event() {
    }

    public Event(final Long songkickId,
                 final Long bandsintownId,
                 final String name,
                 final List<Artist> artists,
                 final Venue venue,
                 final StartTime startTime) {
        this.songkickId = songkickId;
        this.bandsintownId = bandsintownId;
        this.name = name;
        this.artists = artists;
        this.venue = venue;
        this.startTime = startTime;
    }

    public Event(final Event another) {
        id = another.id;
        uuid = another.uuid;
        songkickId = another.songkickId;
        bandsintownId = another.bandsintownId;
        name = another.name;
        artists = another.artists;
        venue = another.venue;
        startTime = another.startTime;
    }

    public Long getSongkickId() {
        return songkickId;
    }

    public Long getBandsintownId() {
        return bandsintownId;
    }

    public String getName() {
        return name;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public Venue getVenue() {
        return venue;
    }

    public StartTime getStartTime() {
        return startTime;
    }

    public void setSongkickId(final Long songkickId) {
        this.songkickId = songkickId;
    }

    public void setBandsintownId(final Long bandsintownId) {
        this.bandsintownId = bandsintownId;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setArtists(final List<Artist> artists) {
        this.artists = artists;
    }

    public void setVenue(final Venue venue) {
        this.venue = venue;
    }

    public void setStartTime(final StartTime startTime) {
        this.startTime = startTime;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other instanceof Event) {
            final Event otherEvent = (Event) other;
            return Objects.equals(uuid, otherEvent.uuid)
                    && Objects.equals(songkickId, otherEvent.getSongkickId())
                    && Objects.equals(bandsintownId, otherEvent.bandsintownId)
                    && Objects.equals(name, otherEvent.name);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result += (uuid != null ? uuid.hashCode() : 0) * 37;
        result += (songkickId != null ? songkickId.hashCode() : 0) * 37;
        result += (bandsintownId != null ? bandsintownId.hashCode() : 0) * 37;
        result += (name != null ? name.hashCode() : 0) * 37;

        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", songkickId=" + songkickId +
                ", bandsintownId=" + bandsintownId +
                ", name='" + name + '\'' +
                ", artists=" + artists +
                ", venue=" + venue +
                ", startTime=" + startTime +
                '}';
    }
}
