package is.ippurpussur.gigfinder.model.neo4j;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@NodeEntity
public class User extends AbstractEntity {

    private String name;

    @Relationship(type = "TRACKING")
    private Set<Artist> trackingArtists;

    private User() {
    }

    public User(final String name) {
        this.name = name;
    }

    public User(final User another) {
        id = another.id;
        uuid = another.uuid;
        name = another.name;
        trackingArtists = another.trackingArtists;
    }

    public String getName() {
        return name;
    }

    public Set<Artist> getTrackingArtists() {
        return trackingArtists;
    }

    public void addTrackingArtists(final Set<Artist> artists) {
        if (trackingArtists == null) {
            trackingArtists = new HashSet<>();
        }
        artists.forEach(artist -> {
            if (!trackingArtists.contains(artist)) {
                trackingArtists.add(artist);
            }
        });
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other instanceof User) {
            final User otherUser = (User) other;
            return Objects.equals(uuid, otherUser.uuid)
                    && Objects.equals(name, otherUser.name)
                    && Objects.equals(trackingArtists, otherUser.trackingArtists);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result += (uuid != null ? uuid.hashCode() : 0) * 37;
        result += (name != null ? name.hashCode() : 0) * 37;
        result += (trackingArtists != null ? trackingArtists.hashCode() : 0) * 37;

        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", name='" + name + '\'' +
                ", trackingArtists=" + trackingArtists +
                '}';
    }
}
