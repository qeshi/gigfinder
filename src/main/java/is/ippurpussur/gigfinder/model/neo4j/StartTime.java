package is.ippurpussur.gigfinder.model.neo4j;

import com.google.gson.annotations.SerializedName;
import is.ippurpussur.gigfinder.model.parser.DomainParser;
import org.neo4j.ogm.annotation.NodeEntity;

import java.util.Objects;

@NodeEntity
public class StartTime extends AbstractEntity {

    @SerializedName("datetime")
    private String dateTime;

    private String date;

    private String time;

    private StartTime() {
    }

    public StartTime(final String dateTime, final String date, final String time) {
        final DomainParser domainParser = new DomainParser();
        this.dateTime = domainParser.formatDateTime(date, time, dateTime);

        this.date = date != null ? date : null;
        this.time = time != null ? time : null;
    }

    public StartTime(final StartTime another) {
        id = another.id;
        uuid = another.uuid;
        dateTime = another.dateTime;
        date = another.date;
        time = another.time;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public void setTime(final String time) {
        this.time = time;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other instanceof StartTime) {
            final StartTime otherStartTime = (StartTime) other;
            return Objects.equals(uuid, otherStartTime.uuid)
                    && Objects.equals(dateTime, otherStartTime.dateTime)
                    && Objects.equals(date, otherStartTime.date)
                    && Objects.equals(time, otherStartTime.time);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result += (uuid != null ? uuid.hashCode() : 0) * 37;
        result += (dateTime != null ? dateTime.hashCode() : 0) * 37;
        result += (date != null ? date.hashCode() : 0) * 37;
        result += (time != null ? time.hashCode() : 0) * 37;

        return result;
    }

    @Override
    public String toString() {
        return "StartTime{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
