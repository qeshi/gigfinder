package is.ippurpussur.gigfinder.model.bandsintown;

import com.google.gson.annotations.SerializedName;

public final class BandsintownVenue {

    @SerializedName("place")
    private String name;

    private String city;

    private String country;

    private Double latitude;

    private Double longitude;

    public BandsintownVenue(
            final String name, final String city, final String country, final Double latitude, final Double longitude) {
        this.name = name;
        this.city = city;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
