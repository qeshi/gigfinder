package is.ippurpussur.gigfinder;

import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.Event;
import is.ippurpussur.gigfinder.model.neo4j.MatchLevel;
import is.ippurpussur.gigfinder.model.neo4j.User;
import is.ippurpussur.gigfinder.model.parser.SongkickParser;
import is.ippurpussur.gigfinder.model.songkick.SongkickEvent;
import is.ippurpussur.gigfinder.neo4j.service.*;
import is.ippurpussur.gigfinder.restclient.bandsintown.BandsintownAPIImpl;
import is.ippurpussur.gigfinder.restclient.lastfm.LastFmAPIImpl;
import is.ippurpussur.gigfinder.restclient.songkick.SongkickAPIImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication
@EnableNeo4jRepositories
@EntityScan("is.ippurpussur.gigfinder.model.neo4j")
public class Application {

    private final Logger log = LoggerFactory.getLogger(Application.class);

    private final UserService userService;
    private final ArtistService artistService;
    private final EventService eventService;
    private final VenueService venueService;
    private final StartTimeService startTimeService;
    private final MatchLevelService matchLevelService;
    private final CoordinatesService coordinatesService;
    private final AreaService areaService;

    @Autowired
    public Application(final UserService userService,
                       final ArtistService artistService,
                       final EventService eventService,
                       final VenueService venueService,
                       final StartTimeService startTimeService,
                       final MatchLevelService matchLevelService,
                       final CoordinatesService coordinatesService,
                       final AreaService areaService) {
        this.userService = userService;
        this.artistService = artistService;
        this.eventService = eventService;
        this.venueService = venueService;
        this.startTimeService = startTimeService;
        this.matchLevelService = matchLevelService;
        this.coordinatesService = coordinatesService;
        this.areaService = areaService;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    // Uncomment the code below to update the database
//    @Bean
//    CommandLineRunner demo() {
//        return args -> {
//            userService.deleteAll();
//            artistService.deleteAll();
//            eventService.deleteAll();
//            venueService.deleteAll();
//            startTimeService.deleteAll();
//            matchLevelService.deleteAll();
//            coordinatesService.deleteAll();
//            areaService.deleteAll();
//
//            areaService.createConstraint();
//            artistService.createConstraint();
//            coordinatesService.createConstraint();
//            eventService.createSongkickIdAsUniqueConstraint();
//            eventService.createBandsintownIdAsUniqueConstraint();
//            startTimeService.createConstraint();
//            venueService.createConstraint();
//            userService.createConstraint();
//
//            final String username = "lindabjalla";
//            final LastFmAPIImpl lastFmAPI = new LastFmAPIImpl();
//            final List<Artist> topArtists = lastFmAPI.getTopArtistsByLastFmUsername(username);
//            final User izumi = new User(username);
//            izumi.addTrackingArtists(new HashSet<>(topArtists));
//            final User user = userService.create(izumi);
//            final Set<Artist> trackingArtists = user.getTrackingArtists();
//            final List<Artist> artists = new ArrayList<>(trackingArtists);
//
//            final String languageCode = "en";
//            final SongkickAPIImpl songkickAPI = new SongkickAPIImpl();
//            final Set<SongkickEvent> upcomingEventsByMultipleArtists = songkickAPI.getUpcomingEventsByMultipleArtists(artists);
//
//            final List<SongkickEvent> songkickEventsWithVenues = songkickAPI.setVenueDetailsToEvents(new ArrayList<>(upcomingEventsByMultipleArtists));
//
//            final SongkickParser songkickParser = new SongkickParser();
//            final List<Event> upcomingEvents = songkickParser.convertSongkickEventsToNodeEntities(songkickEventsWithVenues, languageCode);
//            eventService.createMany(upcomingEvents);
//
//            final int limit = 10;
//            final Set<MatchLevel> matchLevelsOfMultipleArtists = lastFmAPI.createMatchLevelsOfMultipleArtists(artists, limit);
//            matchLevelService.createMany(new ArrayList<>(matchLevelsOfMultipleArtists));
//
//            final BandsintownAPIImpl bandsintownAPI = new BandsintownAPIImpl();
//            final Set<Event> bandsintownEvents =
//                    bandsintownAPI.getUpcomingEventsByMultipleArtists(artists, areaService, languageCode);
//            eventService.createMany(new ArrayList<>(bandsintownEvents));
//        };
//    }
}
